const Mongoose = require("mongoose");

const { Schema } = Mongoose;

const SalesSchema = new Schema({
  products: [
    {
      productId: {
        type: Schema.Types.ObjectId,
        ref: "Product"
      },
      sellPrice: {
        type: Number,
        required: true
      },
      quantity: {
        type: Number,
        required: true
      }
    }
  ],
  discount: {
    type: Number,
    default: 0
  },
  tax: {
    type: Number,
    default: 0
  },
  salesDate: {
    type: Date,
    default: Date.now()
  },
  updatedDate: {
    type: Date
  },
  total: {
    type: Number
  }
});

module.exports = Mongoose.model("Sales", SalesSchema);
