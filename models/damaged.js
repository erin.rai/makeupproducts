const Mongoose = require("mongoose");

const { Schema } = Mongoose;

const DamagedSchema = new Schema({
  productId: {
    type: Schema.Types.ObjectId,
    ref: "Product"
  },
  stockId: {
    type: Schema.Types.ObjectId,
    ref: "Stock"
  },
  quantity: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Mongoose.model("Damaged", DamagedSchema);
