const Mongoose = require("mongoose");

const { Schema } = Mongoose;

const ProductSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  model: {
    type: String
  },
  colors: [
    { color: { type: String, _id: false }, alpha: { _id: false, type: String } }
  ],
  created_at: {
    type: Date,
    default: Date.now(),
    required: true
  },
  updated_at: {
    type: Date
  },
  brandId: {
    type: Schema.Types.ObjectId,
    ref: "Brand"
  },
  description: {
    type: String
  },
  imageUrl: [{ type: String }],
  categoryId: {
    type: Schema.Types.ObjectId,
    ref: "Category"
  },
  subCategoryId: {
    type: Schema.Types.ObjectId
  },
  status: {
    type: Boolean,
    default: true
  }
});

module.exports = Mongoose.model("Product", ProductSchema);
