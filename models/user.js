const Mongoose = require("mongoose");

const { Schema } = Mongoose;

//User Schema
const UserSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  contactno: {
    type: String,
    required: true
  },
  userName: { type: String, required: true },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    enum: ["member"],
    default: "member"
  },
  resetPasswordToken: { type: String },
  resetPasswordExpires: { type: Date }
});

module.exports = Mongoose.model("User", UserSchema);
