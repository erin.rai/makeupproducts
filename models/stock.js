const Mongoose = require("mongoose");

const { Schema } = Mongoose;

const StockSchema = new Schema({
  productId: {
    type: Schema.Types.ObjectId,
    ref: "Product"
  },
  quantity: {
    type: Number,
    required: true
  },
  buyPrice: {
    type: Number,
    required: true
  },
  unitPrice: {
    type: Number,
    required: true
  },
  added_date: {
    type: Date,
    default: Date.now()
  },
  updated_date: {
    type: Date
  }
});

module.exports = Mongoose.model("Stock", StockSchema);
