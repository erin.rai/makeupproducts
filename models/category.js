const Mongoose = require("mongoose");

const { Schema } = Mongoose;

const CategorySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  subcategory: [
    {
      name: { type: String, required: true },
      created: { type: Date, default: Date.now },
      updated: { type: Date },
      status: { type: Boolean, default: true }
    }
  ],
  created: {
    default: Date.now,
    type: Date
  },
  status: {
    type: Boolean,
    default: true
  }
});

module.exports = Mongoose.model("Category", CategorySchema);
