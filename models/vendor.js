const Mongoose = require("mongoose");

const { Schema } = Mongoose;

const VendorSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  address: {
    type: String
  },
  url: {
    type: String
  },
  updated: Date,
  created: {
    type: Date,
    default: Date.now
  },
  active: {
    type: Boolean,
    default: true
  }
});

module.exports = Mongoose.model("Vendor", VendorSchema);
