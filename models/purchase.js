const Mongoose = require("mongoose");

const { Schema } = Mongoose;

const PurchaseSchema = new Schema({
  products: [
    {
      productId: {
        type: Schema.Types.ObjectId,
        ref: "Product"
      },
      buyPrice: {
        type: Number,
        required: true
      },
      sellPrice: {
        type: Number,
        required: true
      },
      quantity: {
        type: Number,
        required: true
      }
    }
  ],
  userId: {
    type: Schema.Types.ObjectId,
    ref: "AdminUser"
  },
  vendorId: {
    type: Schema.Types.ObjectId,
    ref: "Vendor"
  },
  discount: {
    type: Number,
    default: 0
  },
  tax: {
    type: Number,
    default: 0
  },
  purchaseDate: {
    type: Date,
    default: Date.now()
  },
  updatedDate: {
    type: Date
  },
  total: {
    type: Number,
    required: true
  },
  paid: {
    type: Boolean,
    required: true,
    default: false
  }
});

PurchaseSchema.virtual("vendor", {
  ref: "Vendor", // The model to use
  localField: "vendorId", // Find people where `localField`
  foreignField: "name", // is equal to `foreignField`
  // If `justOne` is true, 'members' will be a single doc as opposed to
  // an array. `justOne` is false by default.
  justOne: true // Query options, see http://bit.ly/mongoose-query-options
});

module.exports = Mongoose.model("Purchase", PurchaseSchema);
