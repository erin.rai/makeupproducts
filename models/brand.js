const Mongoose = require("mongoose");

const { Schema } = Mongoose;

const BrandSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  url: {
    type: String
  },
  imageUrl: {
    type: String
  },
  description: {
    type: String
  },
  updated: Date,
  created: {
    type: Date,
    default: Date.now
  },
  active:{
    type:Boolean,
    default:true
  }
});

module.exports = Mongoose.model("Brand", BrandSchema);
