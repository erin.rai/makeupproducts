const Mongoose = require("mongoose");

const { Schema } = Mongoose;

//Admin Schema
const AdminSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    enum: ["admin"],
    default: "admin"
  }
});

module.exports = Mongoose.model("AdminUser", AdminSchema);
