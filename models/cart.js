const Mongoose = require("mongoose");

const { Schema } = Mongoose;

const CartSchema = new Schema({
  product: {
    type: Schema.Types.ObjectId,
    ref: "Stock"
  },
  quantity: Number,
  user: {
    type: Schema.Types.ObjectId,
    ref: "User"
  },
  
});

module.exports = Mongoose.model("Cart", CartSchema                                                 );
