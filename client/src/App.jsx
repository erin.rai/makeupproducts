import React, { Component } from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import jwt_decode from "jwt-decode";
import { Provider } from "react-redux";
import store from "./store";
import history from "./utils/history";

import PrivateRoute from "./components/PrivateRoute/PrivateRoute";
import { setCurrentUser, logoutUser } from "./actions/login/loginActions";
import setAuthToken from "./utils/setAuthToken";

import AdminLayout from "./layouts/Admin.jsx";
import Login from "./views/Login.jsx";

if (localStorage.jwtToken) {
  // Set auth token header auth
  const token = localStorage.jwtToken;
  setAuthToken(token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(token);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // Redirect to login
    window.location.href = "/login";
  }
}
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Switch>
            <Route path="/login" component={Login} />
            <PrivateRoute path="/admin" component={AdminLayout} />
            <Redirect to="/admin/dashboard" />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
