import axios from "axios";
import setAuthToken from "../../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import history from "../../utils/history";

import { LOGIN_ERRORS, CLEAR_LOGIN_ERROR, SET_CURRENT_USER } from "../types";

export const setCurrentUser = decodedUserDetail => {
  return {
    type: SET_CURRENT_USER,
    payload: decodedUserDetail
  };
};

export const clearLoginError = () => {
  return {
    type: CLEAR_LOGIN_ERROR
  };
};

export const loginUser = userDetail => dispatch => {
  dispatch(clearLoginError());
  axios
    .post("/api/admin/login", userDetail)
    .then(res => {
      const { token } = res.data;
      localStorage.setItem("jwtToken", token);
      setAuthToken(token);
      const decodedUserDetail = jwt_decode(token);
      dispatch(setCurrentUser(decodedUserDetail));
    })
    .catch(err => {
      dispatch({
        type: LOGIN_ERRORS,
        payload: err.response.data.payload
      });
    });
};

export const logoutUser = () => dispatch => {
  localStorage.removeItem("jwtToken");
  setAuthToken(false);
  dispatch(setCurrentUser({}));
  history.push("/login");
};
