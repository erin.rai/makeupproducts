import axios from "axios";
import {
  GET_CATEGORY,
  GET_CATEGORIES,
  CATEGORY_LOADING,
  CATEGORY_SUCCESS,
  CLEAR_CATGEORY_ERROR,
  SET_CATEGORY_ERROR,
  GET_TOTAL_CATEGORY,
  GET_ACTIVE_CATEGORY,
  GET_INACTIVE_CATEGORY
} from "../types";

export const setCategorySuccess = status => {
  return {
    type: CATEGORY_SUCCESS,
    payload: status
  };
};

export const setCategoryLoading = status => {
  return {
    type: CATEGORY_LOADING,
    payload: status
  };
};

export const clearCategoryError = () => {
  return {
    type: CLEAR_CATGEORY_ERROR
  };
};

export const setCategoryError = error => {
  return {
    type: SET_CATEGORY_ERROR,
    payload: error
  };
};

export const setCategory = category => {
  return {
    type: GET_CATEGORY,
    payload: category
  };
};

export const setCategories = categories => {
  return {
    type: GET_CATEGORIES,
    payload: categories
  };
};

export const setTotalCategory = total => {
  return {
    type: GET_TOTAL_CATEGORY,
    payload: total
  };
};

export const setInactiveCategoryTotal = total => {
  return {
    type: GET_INACTIVE_CATEGORY,
    payload: total
  };
};

export const setActiveCategoryTotal = total => {
  return {
    type: GET_ACTIVE_CATEGORY,
    payload: total
  };
};

export const getTotalCategory = () => dispatch => {
  dispatch(setCategoryLoading(true));
  axios
    .get(`/api/category/getCategoryCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setCategoryLoading(false));
        dispatch(setTotalCategory(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setCategoryError(err.response.data.payload));
      dispatch(setCategoryLoading(false));
    });
};

export const getActiveCategory = () => dispatch => {
  dispatch(setCategoryLoading(true));
  axios
    .get(`/api/category/getActiveCategoryCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setCategoryLoading(false));
        dispatch(setActiveCategoryTotal(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setCategoryError(err.response.data.payload));
      dispatch(setCategoryLoading(false));
    });
};

export const getInActiveCategory = () => dispatch => {
  dispatch(setCategoryLoading(true));
  axios
    .get(`/api/category/getInActiveCategoryCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setCategoryLoading(false));
        dispatch(setInactiveCategoryTotal(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setCategoryError(err.response.data.payload));
      dispatch(setCategoryLoading(false));
    });
};

export const getCategories = () => dispatch => {
  dispatch(setCategoryLoading(true));
  axios
    .get(`/api/category/getCategories`)
    .then(res => {
      if (res.data.success) {
        dispatch(setCategoryLoading(false));
        dispatch(setCategories(res.data.payload));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setCategoryError(err.response.data.payload));
      dispatch(setCategoryLoading(false));
    });
};

export const getCategory = id => dispatch => {
  dispatch(setCategoryLoading(true));
  axios
    .get(`/api/category/getCategory/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(setCategoryLoading(false));
        dispatch(setCategory(res.data.payload));
      }
    })
    .catch(err => {
      dispatch(setCategoryError(err.response.data.payload));
      dispatch(setCategoryLoading(false));
    });
};

export const addCategory = categoryData => dispatch => {
  dispatch(setCategoryLoading(true));
  dispatch(clearCategoryError());
  axios
    .post(`/api/category/addCategory`, categoryData)
    .then(res => {
      if (res.data.success) {
        dispatch(getCategories());
        dispatch(getTotalCategory());
        dispatch(getActiveCategory());
        dispatch(getInActiveCategory());
        dispatch(setCategorySuccess(true));
        dispatch(setCategoryLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setCategoryLoading(false));
      dispatch(setCategoryError(err.response.data.payload));
    });
};
export const addSubCategory = (subcategoryData, id) => dispatch => {
  dispatch(setCategoryLoading(true));
  dispatch(clearCategoryError());
  axios
    .post(`/api/category/addSubCategory/${id}`, subcategoryData)
    .then(res => {
      if (res.data.success) {
        dispatch(getCategories());
        dispatch(getCategory(id));
        dispatch(getTotalCategory());
        dispatch(getActiveCategory());
        dispatch(getInActiveCategory());
        dispatch(setCategorySuccess(true));
        dispatch(setCategoryLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setCategoryLoading(false));
      dispatch(setCategoryError(err.response.data.payload));
    });
};
export const updateCategory = (categoryData, id) => dispatch => {
  dispatch(setCategoryLoading(true));
  dispatch(clearCategoryError());
  axios
    .patch(`/api/category/updateCategory/${id}`, categoryData)
    .then(res => {
      if (res.data.success) {
        dispatch(getCategories());
        dispatch(getCategory(id));
        dispatch(getTotalCategory());
        dispatch(getActiveCategory());
        dispatch(getInActiveCategory());
        dispatch(setCategorySuccess(true));
        dispatch(setCategoryLoading(false));
      }
    })
    .catch(err => {
      dispatch(setCategoryError(err.response.data.payload));
      dispatch(setCategoryLoading(false));
    });
};

export const updateSubCategory = (categoryData, id) => dispatch => {
  dispatch(setCategoryLoading(true));
  dispatch(clearCategoryError());
  axios
    .patch(`/api/category/updateSubCategory/${id}`, categoryData)
    .then(res => {
      if (res.data.success) {
        dispatch(getCategories());
        dispatch(getCategory(id));
        dispatch(getTotalCategory());
        dispatch(getActiveCategory());
        dispatch(getInActiveCategory());
        dispatch(setCategorySuccess(true));
        dispatch(setCategoryLoading(false));
      }
    })
    .catch(err => {
      dispatch(setCategoryError(err.response.data.payload));
      dispatch(setCategoryLoading(false));
    });
};

export const activateCategory = id => dispatch => {
  axios
    .patch(`/api/category/activateCategory/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(getCategories());
        dispatch(getCategory(id));
        dispatch(getTotalCategory());
        dispatch(getActiveCategory());
        dispatch(getInActiveCategory());
        dispatch(setCategoryLoading(false));
      }
    })
    .catch(err => {
      dispatch(setCategoryLoading(false));
      dispatch(setCategoryError(err.response.data.payload));
    });
};

export const deactivateCategory = id => dispatch => {
  axios
    .patch(`/api/category/deactivateCategory/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(getCategories());
        dispatch(getCategory(id));
        dispatch(getTotalCategory());
        dispatch(getActiveCategory());
        dispatch(getInActiveCategory());
        dispatch(setCategoryLoading(false));
      }
    })
    .catch(err => {
      dispatch(setCategoryLoading(false));
      dispatch(setCategoryError(err.response.data.payload));
    });
};

export const activateSubCategory = (id, data) => dispatch => {
  axios
    .patch(`/api/category/activateSubCategory/${id}`, data)
    .then(res => {
      if (res.data.success) {
        dispatch(getCategories());
        dispatch(getCategory(id));
        dispatch(getTotalCategory());
        dispatch(getActiveCategory());
        dispatch(getInActiveCategory());
        dispatch(setCategoryLoading(false));
      }
    })
    .catch(err => {
      dispatch(setCategoryLoading(false));
      dispatch(setCategoryError(err.response.data.payload));
    });
};
export const deactivateSubCategory = (id, data) => dispatch => {
  axios
    .patch(`/api/category/deactivateSubCategory/${id}`, data)
    .then(res => {
      if (res.data.success) {
        dispatch(getCategories());
        dispatch(getCategory(id));
        dispatch(getTotalCategory());
        dispatch(getActiveCategory());
        dispatch(getInActiveCategory());
        dispatch(setCategoryLoading(false));
      }
    })
    .catch(err => {
      dispatch(setCategoryLoading(false));
      dispatch(setCategoryError(err.response.data.payload));
    });
};
