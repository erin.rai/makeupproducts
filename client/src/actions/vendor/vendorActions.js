import axios from "axios";
import {
  GET_VENDOR,
  GET_VENDORS,
  CLEAR_VENDOR_ERROR,
  VENDOR_SUCCESS,
  SET_VENDOR_ERROR,
  VENDOR_LOADING,
  GET_ACTIVE_VENDOR,
  GET_TOTAL_VENDOR,
  GET_INACTIVE_VENDOR
} from "../types";

export const setVendorSuccess = stat => {
  return {
    type: VENDOR_SUCCESS,
    payload: stat
  };
};

export const setVendorLoading = status => {
  return {
    type: VENDOR_LOADING,
    payload: status
  };
};

export const clearVendorError = () => {
  return {
    type: CLEAR_VENDOR_ERROR
  };
};

export const setVendor = brand => {
  return {
    type: GET_VENDOR,
    payload: brand
  };
};

export const setVendors = brands => {
  return {
    type: GET_VENDORS,
    payload: brands
  };
};

export const setVendorError = errors => {
  return {
    type: SET_VENDOR_ERROR,
    payload: errors
  };
};

export const setTotalVendor = total => {
  return {
    type: GET_TOTAL_VENDOR,
    payload: total
  };
};

export const setInactiveVendorTotal = total => {
  return {
    type: GET_INACTIVE_VENDOR,
    payload: total
  };
};

export const setActiveVendorTotal = total => {
  return {
    type: GET_ACTIVE_VENDOR,
    payload: total
  };
};

export const getTotalVendor = () => dispatch => {
  dispatch(setVendorLoading(true));
  axios
    .get(`/api/vendor/getVendorCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setVendorLoading(false));
        dispatch(setTotalVendor(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setVendorError(err.response.data.payload));
      dispatch(setVendorLoading(false));
    });
};

export const getActiveVendors = () => dispatch => {
  dispatch(setVendorLoading(true));
  axios
    .get(`/api/vendor/getActiveVendorCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setVendorLoading(false));
        dispatch(setActiveVendorTotal(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setVendorError(err.response.data.payload));
      dispatch(setVendorLoading(false));
    });
};

export const getInActiveVendors = () => dispatch => {
  dispatch(setVendorLoading(true));
  axios
    .get(`/api/vendor/getInActiveVendorCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setVendorLoading(false));
        dispatch(setInactiveVendorTotal(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setVendorError(err.response.data.payload));
      dispatch(setVendorLoading(false));
    });
};

export const addVendorData = vendorData => dispatch => {
  axios
    .post("/api/vendor/addVendor", vendorData)
    .then(res => {
      if (res.data.success) {
        dispatch(getVendors());
        dispatch(getTotalVendor());
        dispatch(getInActiveVendors());
        dispatch(getActiveVendors());
        dispatch(setVendorSuccess(true));
        dispatch(setVendorLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setVendorLoading(false));
      dispatch(setVendorError(err.response.data.payload));
    });
};

export const deactivateVendor = id => dispatch => {
  axios
    .patch(`/api/vendor/updateVendor/deactivate/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(getVendors());
        dispatch(getVendor(id));
        dispatch(getTotalVendor());
        dispatch(getInActiveVendors());
        dispatch(getActiveVendors());
        // dispatch(setBrandSuccess(true));
        dispatch(setVendorLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setVendorLoading(false));
      dispatch(setVendorError(err.response.data.payload));
    });
};
export const updateVendor = (vendorData, id) => dispatch => {
  dispatch(clearVendorError());
  dispatch(setVendorLoading(true));
  axios
    .patch(`/api/vendor/updateVendor/${id}`, vendorData)
    .then(res => {
      if (res.data.success) {
        dispatch(getVendors());
        dispatch(getTotalVendor());
        dispatch(getInActiveVendors());
        dispatch(getActiveVendors());
        dispatch(setVendorSuccess(true));
        dispatch(setVendorLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setVendorError(err.response.data.payload));
      dispatch(setVendorLoading(false));
    });
};
export const activateVendor = id => dispatch => {
  axios
    .patch(`/api/vendor/updateVendor/activate/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(getVendors());
        dispatch(getVendor(id));
        dispatch(getTotalVendor());
        dispatch(getInActiveVendors());
        dispatch(getActiveVendors());
        // dispatch(setBrandSuccess(true));
        dispatch(setVendorLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setVendorLoading(false));
      dispatch(setVendorError(err.response.data.payload));
    });
};

export const getVendors = () => dispatch => {
  dispatch(setVendorLoading(true));
  axios
    .get(`/api/vendor/getVendors`)
    .then(res => {
      if (res.data.success) {
        dispatch(setVendorLoading(false));
        dispatch(setVendors(res.data.payload));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setVendorError(err.response.data.payload));
      dispatch(setVendorLoading(false));
    });
};

export const getVendor = id => dispatch => {
  dispatch(setVendorLoading(true));
  console.log(id);
  axios
    .get(`/api/vendor/getVendor/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(setVendorLoading(false));
        dispatch(setVendor(res.data.payload));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setVendorError(err.response.data.payload));
      dispatch(setVendorLoading(false));
    });
};
