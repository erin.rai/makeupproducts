import axios from "axios";
import {
  PRODUCT_LOADING,
  PRODUCT_SUCCESS,
  SET_PRODUCT_ERROR,
  CLEAR_PRODUCT_ERROR,
  UPDATE_PRODUCT,
  GET_PORDUCT,
  GET_CATEGORY_LIST,
  GET_BRAND_LIST,
  GET_PRODUCTS,
  GET_TOTAL_PRODUCT,
  GET_INACTIVE_PRODUCT,
  GET_ACTIVE_PRODUCT
} from "../types";

export const setProductSuccess = status => {
  return {
    type: PRODUCT_SUCCESS,
    payload: status
  };
};

export const setCategoryList = categories => {
  return {
    type: GET_CATEGORY_LIST,
    payload: categories
  };
};

export const setBrandList = brands => {
  return {
    type: GET_BRAND_LIST,
    payload: brands
  };
};

export const setProductLoading = status => {
  return {
    type: PRODUCT_LOADING,
    payload: status
  };
};

export const setProductError = error => {
  return {
    type: SET_PRODUCT_ERROR,
    payload: error
  };
};

export const setProduct = product => {
  return {
    type: GET_PORDUCT,
    payload: product
  };
};

export const setProducts = products => {
  return {
    type: GET_PRODUCTS,
    payload: products
  };
};

export const clearProductError = () => {
  return {
    type: CLEAR_PRODUCT_ERROR
  };
};

export const setTotalProduct = total => {
  return {
    type: GET_TOTAL_PRODUCT,
    payload: total
  };
};

export const setInactiveProductTotal = total => {
  return {
    type: GET_INACTIVE_PRODUCT,
    payload: total
  };
};

export const setActiveProductTotal = total => {
  return {
    type: GET_ACTIVE_PRODUCT,
    payload: total
  };
};

export const getTotalProduct = () => dispatch => {
  dispatch(setProductLoading(true));
  axios
    .get(`/api/product/getProductCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setProductLoading(false));
        dispatch(setTotalProduct(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setProductError(err.response.data.payload));
      dispatch(setProductLoading(false));
    });
};

export const getActiveProducts = () => dispatch => {
  dispatch(setProductLoading(true));
  axios
    .get(`/api/product/getActiveProductCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setProductLoading(false));
        dispatch(setActiveProductTotal(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setProductError(err.response.data.payload));
      dispatch(setProductLoading(false));
    });
};

export const getInActiveProducts = () => dispatch => {
  dispatch(setProductLoading(true));
  axios
    .get(`/api/product/getInActiveProductCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setProductLoading(false));
        dispatch(setInactiveProductTotal(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setProductError(err.response.data.payload));
      dispatch(setProductLoading(false));
    });
};

export const getProducts = () => dispatch => {
  dispatch(setProductLoading(true));
  dispatch(clearProductError());
  axios
    .get("/api/product/getProducts")
    .then(res => {
      if (res.data.success) {
        dispatch(setProductLoading(false));
        dispatch(setProducts(res.data.payload));
      }
    })
    .catch(err => {
      dispatch(setProductError(err.response.data.payload));
      dispatch(setProductLoading(false));
    });
};

export const getCategoriesList = () => dispatch => {
  dispatch(setProductLoading(true));
  dispatch(clearProductError());
  axios
    .get("/api/category/getAllCategoryList")
    .then(res => {
      if (res.data.success) {
        dispatch(setProductLoading(false));
        dispatch(setCategoryList(res.data.payload));
      }
    })
    .catch(err => {
      dispatch(setProductError(err.response.data.payload));
      dispatch(setProductLoading(false));
    });
};

export const getBrandList = () => dispatch => {
  dispatch(setProductLoading(true));
  dispatch(clearProductError());
  axios
    .get("/api/brand/getAllBrandList")
    .then(res => {
      if (res.data.success) {
        dispatch(setProductLoading(false));
        dispatch(setBrandList(res.data.payload));
      }
    })
    .catch(err => {
      dispatch(setProductError(err.response.data.payload));
      dispatch(setProductLoading(false));
    });
};

export const getProduct = id => dispatch => {
  dispatch(setProductLoading(false));
  dispatch(clearProductError());
  axios
    .get(`/api/product/getProduct/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(setProductLoading(false));
        dispatch(setProduct(res.data.payload));
      }
    })
    .catch(err => {
      dispatch(setProductError(err.response.data.payload));
      dispatch(setProductLoading(false));
    });
};

export const addProductData = productData => dispatch => {
  axios
    .post(`/api/product/addProduct`, productData)
    .then(res => {
      if (res.data.success) {
        dispatch(getProducts());
        dispatch(getTotalProduct());
        dispatch(getActiveProducts());
        dispatch(getInActiveProducts());
        dispatch(setProductSuccess(true));
        dispatch(setProductLoading(false));
      }
    })
    .catch(err => {
      dispatch(setProductLoading(false));
      dispatch(setProductError(err.response.data.payload));
    });
};

export const addProduct = (productImages, productData) => dispatch => {
  dispatch(setProductLoading(true));
  dispatch(clearProductError());
  if (productImages.length === 0) {
    dispatch(addProductData(productData));
  } else {
    console.log(productImages);
    const formData = new FormData();
    productImages.forEach(file => {
      formData.append("productimages", file);
    });
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };

    axios
      .post("/api/product/productImages", formData, config)
      .then(res => {
        if (res.data.success) {
          productData["imageUrl"] = res.data.payload;
          dispatch(addProductData(productData));
        }
      })
      .catch(err => {
        dispatch(setProductError(err.response.data.payload));
        dispatch(setProductLoading(false));
      });
  }
};

export const updateProductData = (productData, id) => dispatch => {
  axios
    .patch(`/api/product/updateProduct/${id}`, productData)
    .then(res => {
      if (res.data.success) {
        dispatch(getProducts());
        dispatch(getProduct(id));
        dispatch(getTotalProduct());
        dispatch(getActiveProducts());
        dispatch(getInActiveProducts());
        dispatch(setProductSuccess(true));
        dispatch(setProductLoading(false));
      }
    })
    .catch(err => {
      dispatch(setProductError(err.response.data.payload));
      dispatch(setProductLoading(false));
    });
};

export const updateProduct = (productImages, productData, id) => dispatch => {
  dispatch(setProductLoading(true));
  dispatch(clearProductError());
  if (productImages.length === 0) {
    dispatch(updateProductData(productData, id));
  } else {
    const formData = new FormData();
    productImages.forEach(file => {
      formData.append("productimages", file);
    });
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    axios
      .post("/api/product/productImages", formData, config)
      .then(res => {
        if (res.data.success) {
          productData["imageUrl"] = res.data.payload;
          dispatch(updateProductData(productData, id));
        }
      })
      .catch(err => {
        dispatch(setProductError(err.response.data.payload));
        dispatch(setProductLoading(false));
      });
  }
};

export const activateProduct = id => dispatch => {
  axios
    .patch(`/api/product/activateProduct/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(getProducts());
        dispatch(getProduct(id));
        dispatch(getTotalProduct());
        dispatch(getActiveProducts());
        dispatch(getInActiveProducts());
        dispatch(setProductLoading(false));
      }
    })
    .catch(err => {
      dispatch(setProductLoading(false));
      dispatch(setProductError(err.response.data.payload));
    });
};

export const deactivateProduct = id => dispatch => {
  axios
    .patch(`/api/product/deactivateProduct/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(getProducts());
        dispatch(getProduct(id));
        dispatch(getTotalProduct());
        dispatch(getActiveProducts());
        dispatch(getInActiveProducts());
        dispatch(setProductLoading(false));
      }
    })
    .catch(err => {
      dispatch(setProductLoading(false));
      dispatch(setProductError(err.response.data.payload));
    });
};
