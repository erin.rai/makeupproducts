import axios from "axios";
import { GET_STOCKS, STOCK_LOADING } from "../types";

export const setStockLoading = status => {
  return {
    type: STOCK_LOADING,
    payload: status
  };
};
export const setStock = stocks => {
  return {
    type: GET_STOCKS,
    payload: stocks
  };
};

export const getStocks = () => dispatch => {
  dispatch(setStockLoading(true));
  axios
    .get(`/api/stock/getStocks`)
    .then(res => {
      if (res.data.success) {
        dispatch(setStockLoading(false));
        dispatch(setStock(res.data.payload));
      }
    })
    .catch(err => {
      console.log(err);
      //   dispatch(setStock(err.response.data.payload));
      dispatch(setStockLoading(false));
    });
};
