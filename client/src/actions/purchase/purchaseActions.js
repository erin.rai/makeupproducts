import axios from "axios";
import {
  GET_PURCHASE,
  GET_PURCHASES,
  SET_PURCHASE_ERROR,
  CLEAR_PURCHASE_ERROR,
  PURCHASE_SUCCESS,
  PURCHASE_LOADING,
  GET_PRODUCT_LIST,
  GET_VENDOR_LIST,
  GET_TODAY_PURCHASE,
  GET_MONTHLY_PURCHASE,
  GET_WEEKLY_PURCHASE,
  GET_TOTAL_UNPAID
} from "../types";

export const setPurchaseSuccess = status => {
  return {
    type: PURCHASE_SUCCESS,
    payload: status
  };
};

export const setPurchaseLoading = status => {
  return {
    type: PURCHASE_LOADING,
    payload: status
  };
};

export const clearPurchaseError = () => {
  return {
    type: CLEAR_PURCHASE_ERROR
  };
};

export const setPurchaseError = error => {
  return {
    type: SET_PURCHASE_ERROR,
    payload: error
  };
};

export const setPurchase = purchase => {
  return {
    type: GET_PURCHASE,
    payload: purchase
  };
};

export const setPurchases = purchases => {
  return {
    type: GET_PURCHASES,
    payload: purchases
  };
};

export const setProductList = products => {
  return {
    type: GET_PRODUCT_LIST,
    payload: products
  };
};

export const setVendorList = vendors => {
  return {
    type: GET_VENDOR_LIST,
    payload: vendors
  };
};
export const setDailyPurchase = total => {
  return {
    type: GET_TODAY_PURCHASE,
    payload: total
  };
};

export const setWeeklyPurchase = total => {
  return {
    type: GET_WEEKLY_PURCHASE,
    payload: total
  };
};

export const setMonthlyPurchase = total => {
  return {
    type: GET_MONTHLY_PURCHASE,
    payload: total
  };
};

export const setTotalUnpaid = total => {
  return {
    type: GET_TOTAL_UNPAID,
    payload: total
  };
};

export const getDailyPurchase = () => dispatch => {
  dispatch(setPurchaseLoading(true));
  axios
    .get(`/api/purchase/todayPurchase`)
    .then(res => {
      if (res.data.success) {
        dispatch(setPurchaseLoading(false));
        dispatch(setDailyPurchase(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setPurchaseError(err.response.data.payload));
      dispatch(setPurchaseLoading(false));
    });
};

export const getWeelyPurchase = () => dispatch => {
  dispatch(setPurchaseLoading(true));
  axios
    .get(`/api/purchase/weeklyPurchase`)
    .then(res => {
      if (res.data.success) {
        dispatch(setPurchaseLoading(false));
        dispatch(setWeeklyPurchase(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setPurchaseError(err.response.data.payload));
      dispatch(setPurchaseLoading(false));
    });
};

export const getMonthlyPurchase = () => dispatch => {
  dispatch(setPurchaseLoading(true));
  axios
    .get(`/api/purchase/monthlyPurchase`)
    .then(res => {
      if (res.data.success) {
        dispatch(setPurchaseLoading(false));
        dispatch(setMonthlyPurchase(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setPurchaseError(err.response.data.payload));
      dispatch(setPurchaseLoading(false));
    });
};
export const getTotalUnpaidPurchase = () => dispatch => {
  dispatch(setPurchaseLoading(true));
  axios
    .get(`/api/purchase/totalUnpaid`)
    .then(res => {
      if (res.data.success) {
        dispatch(setPurchaseLoading(false));
        dispatch(setTotalUnpaid(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setPurchaseError(err.response.data.payload));
      dispatch(setPurchaseLoading(false));
    });
};

export const getPurchases = () => dispatch => {
  dispatch(setPurchaseLoading(true));
  axios
    .get(`/api/purchase/getPurchases`)
    .then(res => {
      if (res.data.success) {
        dispatch(setPurchaseLoading(false));
        dispatch(setPurchases(res.data.payload));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setPurchaseError(err.response.data.payload));
      dispatch(setPurchaseLoading(false));
    });
};

export const getPurchase = id => dispatch => {
  dispatch(setPurchaseLoading(true));
  axios
    .get(`/api/purchase/getPurchase/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(setPurchaseLoading(false));
        dispatch(setPurchase(res.data.payload));
      }
    })
    .catch(err => {
      dispatch(setPurchaseError(err.response.data.payload));
      dispatch(setPurchaseLoading(false));
    });
};

export const addPurchase = purchaseData => dispatch => {
  dispatch(setPurchaseLoading(true));
  dispatch(clearPurchaseError());
  axios
    .post(`/api/purchase/addPurchase`, purchaseData)
    .then(res => {
      if (res.data.success) {
        dispatch(getPurchases());
        dispatch(getDailyPurchase());
        dispatch(getWeelyPurchase());
        dispatch(getMonthlyPurchase());
        dispatch(getTotalUnpaidPurchase());
        dispatch(setPurchaseSuccess(true));
        dispatch(setPurchaseLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setPurchaseLoading(false));
      dispatch(setPurchaseError(err.response.data.payload));
    });
};

export const getProductList = () => dispatch => {
  dispatch(setPurchaseLoading(true));
  dispatch(clearPurchaseError());
  axios
    .get("/api/purchase/getAllProductList")
    .then(res => {
      if (res.data.success) {
        dispatch(setPurchaseLoading(false));
        dispatch(setProductList(res.data.payload));
      }
    })
    .catch(err => {
      dispatch(setPurchaseError(err.response.data.payload));
      dispatch(setPurchaseLoading(false));
    });
};

export const getVendorList = () => dispatch => {
  dispatch(setPurchaseLoading(true));
  dispatch(clearPurchaseError());
  axios
    .get("/api/purchase/getAllVendorList")
    .then(res => {
      if (res.data.success) {
        dispatch(setPurchaseLoading(false));
        dispatch(setVendorList(res.data.payload));
      }
    })
    .catch(err => {
      dispatch(setPurchaseError(err.response.data.payload));
      dispatch(setPurchaseLoading(false));
    });
};

export const updatePurchase = id => dispatch => {
  axios
    .patch(`/api/purchase/updatePurchase/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(getPurchases());
        dispatch(getPurchase(id));
        dispatch(getDailyPurchase());
        dispatch(getWeelyPurchase());
        dispatch(getMonthlyPurchase());
        dispatch(getTotalUnpaidPurchase());
        dispatch(setPurchaseSuccess(true));
        dispatch(setPurchaseLoading(false));
      }
    })
    .catch(err => {
      dispatch(setPurchaseError(err.response.data.payload));
      dispatch(setPurchaseLoading(false));
    });
};
