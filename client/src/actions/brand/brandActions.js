import axios from "axios";
import {
  GET_BRAND,
  GET_BRANDS,
  CLEAR_BRAND_ERROR,
  BRAND_SUCCESS,
  SET_BRAND_ERROR,
  BRAND_LOADING,
  GET_ACTIVE_BRAND,
  GET_INACTIVE_BRAND,
  GET_TOTAL_BRAND
} from "../types";

export const setBrandSuccess = stat => {
  return {
    type: BRAND_SUCCESS,
    payload: stat
  };
};

export const setBrandLoading = status => {
  return {
    type: BRAND_LOADING,
    payload: status
  };
};

export const clearBrandError = () => {
  return {
    type: CLEAR_BRAND_ERROR
  };
};

export const setBrand = brand => {
  return {
    type: GET_BRAND,
    payload: brand
  };
};

export const setBrands = brands => {
  return {
    type: GET_BRANDS,
    payload: brands
  };
};

export const setBrandError = errors => {
  return {
    type: SET_BRAND_ERROR,
    payload: errors
  };
};

export const setTotalBrand = total => {
  return {
    type: GET_TOTAL_BRAND,
    payload: total
  };
};

export const setInactiveBrandTotal = total => {
  return {
    type: GET_INACTIVE_BRAND,
    payload: total
  };
};

export const setActiveBrandTotal = total => {
  return {
    type: GET_ACTIVE_BRAND,
    payload: total
  };
};

export const getTotalBrands = () => dispatch => {
  dispatch(setBrandLoading(true));
  axios
    .get(`/api/brand/getBrandCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setBrandLoading(false));
        dispatch(setTotalBrand(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setBrandError(err.response.data.payload));
      dispatch(setBrandLoading(false));
    });
};

export const getActiveBrands = () => dispatch => {
  dispatch(setBrandLoading(true));
  axios
    .get(`/api/brand/getActiveBrandCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setBrandLoading(false));
        dispatch(setActiveBrandTotal(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setBrandError(err.response.data.payload));
      dispatch(setBrandLoading(false));
    });
};

export const getInActiveBrands = () => dispatch => {
  dispatch(setBrandLoading(true));
  axios
    .get(`/api/brand/getInActiveBrandCount`)
    .then(res => {
      if (res.data.success) {
        dispatch(setBrandLoading(false));
        dispatch(setInactiveBrandTotal(res.data.payload.count));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setBrandError(err.response.data.payload));
      dispatch(setBrandLoading(false));
    });
};

export const addBrandData = brandData => dispatch => {
  axios
    .post("/api/brand/addBrand", brandData)
    .then(res => {
      if (res.data.success) {
        dispatch(getBrands());
        dispatch(getTotalBrands());
        dispatch(getInActiveBrands());
        dispatch(getActiveBrands());
        dispatch(setBrandSuccess(true));
        dispatch(setBrandLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setBrandLoading(false));
      dispatch(setBrandError(err.response.data.payload));
    });
};

export const deactivateBrand = id => dispatch => {
  axios
    .patch(`/api/brand/updateBrand/deactivate/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(getBrands());
        dispatch(getBrand(id));
        dispatch(getTotalBrands());
        dispatch(getInActiveBrands());
        dispatch(getActiveBrands());
        dispatch(setBrandLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setBrandLoading(false));
      dispatch(setBrandError(err.response.data.payload));
    });
};

export const activateBrand = id => dispatch => {
  axios
    .patch(`/api/brand/updateBrand/activate/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(getBrands());
        dispatch(getBrand(id));
        dispatch(getTotalBrands());
        dispatch(getInActiveBrands());
        dispatch(getActiveBrands());
        dispatch(setBrandLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setBrandLoading(false));
      dispatch(setBrandError(err.response.data.payload));
    });
};
export const addBrand = (brandImage, brandData) => dispatch => {
  dispatch(setBrandLoading(true));
  dispatch(clearBrandError());

  if (brandImage === "") {
    dispatch(addBrandData(brandData));
  } else {
    const formData = new FormData();
    formData.append("brandimage", brandImage);
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };

    axios
      .post("/api/brand/brandImage", formData, config)
      .then(res => {
        if (res.data.success) {
          brandData["imageUrl"] = res.data.payload.url;
          dispatch(addBrandData(brandData));
        }
      })
      .catch(err => {
        dispatch(setBrandError(err.response.data.payload));
        dispatch(setBrandLoading(false));
      });
  }
};

export const updateBrand = (brandImage, brandData, id) => dispatch => {
  dispatch(setBrandLoading(true));
  dispatch(clearBrandError());

  if (brandImage === "") {
    dispatch(updateBrandData(brandData, id));
  } else {
    const formData = new FormData();
    formData.append("brandimage", brandImage);
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };

    axios
      .post("/api/brand/brandImage", formData, config)
      .then(res => {
        if (res.data.success) {
          brandData["imageUrl"] = res.data.payload.url;
          dispatch(updateBrandData(brandData, id));
        }
      })
      .catch(err => {
        dispatch(setBrandError(err.response.data.payload));
        dispatch(setBrandLoading(false));
      });
  }
};

export const updateBrandData = (brandData, id) => dispatch => {
  dispatch(clearBrandError());
  dispatch(setBrandLoading(true));
  axios
    .patch(`/api/brand/updateBrand/${id}`, brandData)
    .then(res => {
      if (res.data.success) {
        dispatch(getBrands());
        dispatch(getTotalBrands());
        dispatch(getInActiveBrands());
        dispatch(getActiveBrands());
        dispatch(setBrandSuccess(true));
        dispatch(setBrandLoading(false));
      }
    })
    .catch(err => {
      console.log(err.response.data.payload);
      dispatch(setBrandError(err.response.data.payload));
      dispatch(setBrandLoading(false));
    });
};
export const getBrands = () => dispatch => {
  dispatch(setBrandLoading(true));
  axios
    .get(`/api/brand/getBrands`)
    .then(res => {
      if (res.data.success) {
        dispatch(setBrandLoading(false));
        dispatch(setBrands(res.data.payload));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setBrandError(err.response.data.payload));
      dispatch(setBrandLoading(false));
    });
};

export const getBrand = id => dispatch => {
  dispatch(setBrandLoading(true));
  console.log(id);
  axios
    .get(`/api/brand/getBrand/${id}`)
    .then(res => {
      if (res.data.success) {
        dispatch(setBrandLoading(false));
        dispatch(setBrand(res.data.payload));
      }
    })
    .catch(err => {
      console.log(err);
      dispatch(setBrandError(err.response.data.payload));
      dispatch(setBrandLoading(false));
    });
};
