import React, { useState, useEffect } from "react";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  FormFeedback,
  Row
} from "reactstrap";
import { connect } from "react-redux";
import {
  updateCategory,
  deactivateCategory,
  deactivateSubCategory,
  activateCategory,
  activateSubCategory,
  setCategorySuccess,
  updateSubCategory,
  setCategoryError,
  addSubCategory
} from "../../actions/category/categoryActions";

const AddSubCategory = ({
  category,
  cancelAddSubToggle,
  updateCategory,
  deactivateCategory,
  deactivateSubCategory,
  activateCategory,
  updateSubCategory,
  activateSubCategory,
  addSubCategory,
  setCategorySuccess
}) => {
  const [subcategory, setSubcategoryNew] = useState([]);
  const [newSubCategory, setNewSubCategory] = useState("");
  const [categoryName, setCategoryName] = useState("");
  const [successCategory, setSuccessCategory] = useState("");
  const [successSubCategory, setSuccessSubCategory] = useState("");
  const [sameNameCategory, setSameNameCategory] = useState("");
  const [sameNameSubCategory, setSameNameSubCategory] = useState("");
  const [errorCatgory, setErrorCategory] = useState(null);
  const [errorSubCategory, setErrorSubCategory] = useState(null);
  const [index, setIndex] = useState(null);
  const [categoryStatus, setCategoryStatus] = useState(false);
  const [categorySubStatus, setCategorySubStatus] = useState(false);

  useEffect(() => {
    setCategoryName(
      category.category.name !== undefined ? category.category.name : ""
    );
    setSubcategoryNew(
      category.category.subcategory !== undefined
        ? category.category.subcategory
        : []
    );
  }, [category.category]);

  useEffect(() => {
    if (category.success) {
      if (categoryStatus) {
        setSuccessCategory("Category Updated Successfully!");
        setTimeout(() => {
          setSuccessCategory("");
          setCategorySubStatus(false);
          setCategorySuccess(false);
        }, 2000);
      } else if (categorySubStatus) {
        setSuccessSubCategory("Sub Category Added Successfully!");
        setSubcategoryNew([]);
        setTimeout(() => {
          setSuccessSubCategory("");
          setCategoryStatus(false);
          setCategorySuccess(false);
        }, 2000);
      } else {
        setSuccessSubCategory("Sub Category Updated Successfully!");
        setTimeout(() => {
          setSuccessSubCategory("");
          setIndex(null);
          setCategorySuccess(false);
        }, 2000);
      }
    }
  }, [category.success, setCategorySuccess, categoryStatus, categorySubStatus]);

  useEffect(() => {
    if (categoryStatus) {
      setErrorCategory(category.error);
    } else {
      setErrorSubCategory(category.error);
    }
  }, [category.error, categoryStatus]);

  function handleSubCategoryChange(i, event) {
    const values = [...subcategory];
    values[i].name = event.target.value;
    setSubcategoryNew(values);
  }

  function handleNewSubChange(event) {
    setNewSubCategory(event.target.value);
  }

  function handleCategoryName(event) {
    setCategoryName(event.target.value);
  }

  function updateCategoryName() {
    if (category.category.name === categoryName) {
      setSameNameCategory("No changed data in Category!");
      console.log(sameNameCategory);
      setTimeout(() => {
        setSameNameCategory("");
      }, 2000);
    } else {
      const data = {
        name: categoryName
      };
      setCategoryStatus(true);
      updateCategory(data, category.category._id);

      // setCategoryError(error);
      // console.log(errorCatgory);
    }
  }

  function updateSubCategoryName(i) {
    console.log(i);
    setIndex(i);
    // if (category.category.subcategory[i].name === subcategory[i].name) {
    //   setSameNameSubCategory("No changed data in Sub Category!");
    //   setTimeout(() => {
    //     setSameNameSubCategory("");
    //   }, 2000);
    // } else {
    const data = {
      subId: subcategory[i]._id,
      name: subcategory[i].name
    };
    updateSubCategory(data, category.category._id);
    // }
  }

  function addNewSubcategory() {
    setCategorySubStatus(true);
    const data = {
      name: newSubCategory
    };
    addSubCategory(data, category.category._id);
  }
  return (
    <Col md="12">
      <Card>
        <CardHeader>
          <Row>
            <Col md="3">
              <CardTitle tag="h4">Edit Sub Category</CardTitle>
            </Col>
            <Col mod="4" className="text-right" style={{ marginRight: 10 }}>
              <Button
                className="btn-round"
                color="danger"
                outline
                onClick={() => cancelAddSubToggle()}
              >
                Cancel
              </Button>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Form>
            <FormGroup>
              <Label for="category">Category</Label>
              <Row style={{ alignItems: "center" }}>
                <Col md="8">
                  <Input
                    type="text"
                    id="brandName"
                    value={categoryName}
                    placeholder="YSL Beauty"
                    onChange={handleCategoryName}
                    invalid={errorCatgory && errorCatgory.category}
                  />
                  {errorCatgory && errorCatgory.category ? (
                    <FormFeedback invalid>{errorCatgory.category}</FormFeedback>
                  ) : null}
                </Col>
                <Col md="4">
                  <Row>
                    <Button
                      color="primary"
                      style={{ width: "100px" }}
                      onClick={updateCategoryName}
                    >
                      Update
                    </Button>
                    {category.category.status ? (
                      <Button
                        color="danger"
                        style={{ width: "150px" }}
                        onClick={() =>
                          deactivateCategory(category.category._id)
                        }
                      >
                        Deactivate
                      </Button>
                    ) : (
                      <Button
                        color="success"
                        style={{ width: "150px" }}
                        onClick={() => activateCategory(category.category._id)}
                      >
                        Activate
                      </Button>
                    )}
                  </Row>
                </Col>
                {successCategory !== "" ? (
                  <Row>
                    <Col style={{ marginLeft: "10px" }}>
                      <Alert color="success">{successCategory}</Alert>
                    </Col>
                  </Row>
                ) : (
                  ""
                )}
                {sameNameCategory !== "" ? (
                  <Row>
                    <Col style={{ marginLeft: "10px" }}>
                      <Alert color="danger">{sameNameCategory}</Alert>
                    </Col>
                  </Row>
                ) : (
                  ""
                )}
              </Row>
            </FormGroup>
            <FormGroup>
              <Label for="category">Subcategories</Label>
              {subcategory.map((subcat, i) => (
                <Row key={i}>
                  <Col md="8" xs="8">
                    <Input
                      style={{ marginTop: "8px" }}
                      type="text"
                      name="brandName"
                      id="brandName"
                      placeholder="YSL Beauty"
                      value={subcat.name}
                      onChange={e => handleSubCategoryChange(i, e)}
                      invalid={
                        index != null &&
                        i === index &&
                        errorSubCategory &&
                        errorSubCategory.subcategory
                      }
                    />
                    {index != null &&
                    i === index &&
                    errorSubCategory &&
                    errorSubCategory.subcategory ? (
                      <FormFeedback invalid>
                        {errorSubCategory.subcategory}
                      </FormFeedback>
                    ) : null}
                    {index != null &&
                    i === index &&
                    sameNameSubCategory !== "" ? (
                      <Row>
                        <Col md="10" style={{ marginTop: "10px" }}>
                          <Alert color="danger">{sameNameSubCategory}</Alert>
                        </Col>
                      </Row>
                    ) : (
                      ""
                    )}
                    {index != null &&
                    i === index &&
                    successSubCategory !== "" ? (
                      <Row>
                        <Col md="10">
                          <Alert color="success">{successSubCategory}</Alert>
                        </Col>
                      </Row>
                    ) : (
                      ""
                    )}
                  </Col>
                  <Col md="4" xs="4">
                    <Row>
                      <Button
                        color="primary"
                        style={{ width: "100px" }}
                        onClick={() => updateSubCategoryName(i)}
                      >
                        Update
                      </Button>
                      {subcat.status ? (
                        <Button
                          color="danger"
                          style={{ width: "150px" }}
                          onClick={() => {
                            const data = {
                              subId: subcategory[i]._id
                            };
                            deactivateSubCategory(category.category._id, data);
                          }}
                        >
                          Deactivate
                        </Button>
                      ) : (
                        <Button
                          color="success"
                          style={{ width: "150px" }}
                          onClick={() => {
                            const data = {
                              subId: subcategory[i]._id
                            };
                            activateSubCategory(category.category._id, data);
                          }}
                        >
                          Activate
                        </Button>
                      )}
                    </Row>
                  </Col>
                </Row>
              ))}
            </FormGroup>
            <FormGroup>
              <Label for="category">New Subcategory</Label>
              <Row style={{ alignItems: "center" }}>
                <Col md="8" xs="8">
                  <Input
                    type="text"
                    name="subCategory"
                    id="subCategoryName"
                    value={newSubCategory}
                    placeholder="YSL Beauty"
                    onChange={e => handleNewSubChange(e)}
                    invalid={
                      categorySubStatus &&
                      errorSubCategory &&
                      errorSubCategory.subcategory
                    }
                  />
                  {categorySubStatus &&
                  errorSubCategory &&
                  errorSubCategory.subcategory ? (
                    <FormFeedback invalid>
                      {errorSubCategory.subcategory}
                    </FormFeedback>
                  ) : null}
                </Col>
                <Col md="4" xs="4">
                  <Button
                    style={{ width: "240px" }}
                    color="primary"
                    onClick={addNewSubcategory}
                  >
                    Add
                  </Button>
                </Col>
              </Row>
              {categorySubStatus && successSubCategory !== "" ? (
                <Row>
                  <Col md="10">
                    <Alert color="success">{successSubCategory}</Alert>
                  </Col>
                </Row>
              ) : (
                ""
              )}
            </FormGroup>
            <div style={{ height: "10px" }} />

            {/* <Button color="primary">Submit</Button> */}
          </Form>
        </CardBody>
      </Card>
    </Col>
  );
};
const mapStateToProps = state => ({
  login: state.login,
  category: state.category
});
export default connect(mapStateToProps, {
  updateCategory,
  activateCategory,
  activateSubCategory,
  deactivateCategory,
  deactivateSubCategory,
  setCategorySuccess,
  addSubCategory,
  updateSubCategory
})(AddSubCategory);
