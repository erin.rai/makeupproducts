import React, { useState, useEffect } from "react";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Row,
  Alert
} from "reactstrap";
import { connect } from "react-redux";
import {
  addCategory,
  clearCategoryError
} from "../../actions/category/categoryActions";

const AddCategory = ({
  addCategory,
  clearCategoryError,
  category,
  cancelCatToggle
}) => {
  const [categoryName, setCategoryName] = useState("");
  const [error, setError] = useState(null);
  const [success, setSuccessMessage] = useState("");

  const handleChangeName = event => {
    setCategoryName(event.target.value);
  };

  const submitCategory = e => {
    e.preventDefault();
    const data = {
      name: categoryName
    };
    addCategory(data);
  };

  useEffect(() => {
    setError(category.error);
  }, [category.error]);

  useEffect(() => {
    clearCategoryError();
  }, [clearCategoryError]);

  useEffect(() => {
    if (category.success) {
      setSuccessMessage("Category Added Successfully!");
      setTimeout(() => {
        setSuccessMessage("");
        setCategoryName("");
        setError("");
      }, 2000);
    }
  }, [category.success, cancelCatToggle]);

  return (
    <Col md="12">
      <Card>
        <CardHeader>
          <Row>
            <Col md="3">
              <CardTitle tag="h4">Add New Category</CardTitle>
            </Col>
            <Col mod="4" className="text-right" style={{ marginRight: 10 }}>
              <Button
                className="btn-round"
                color="danger"
                outline
                onClick={() => cancelCatToggle()}
              >
                Cancel
              </Button>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Form>
            <FormGroup>
              <Label for="category">Category</Label>
              <Input
                type="text"
                name="brandName"
                id="brandName"
                value={categoryName}
                onChange={handleChangeName}
                placeholder="YSL Beauty"
                invalid={error && error.category ? true : false}
              />
              {error && error.category ? (
                <FormFeedback invalid>{error.category}</FormFeedback>
              ) : null}
            </FormGroup>

            <div style={{ height: "10px" }} />
            {success !== "" ? <Alert color="success">{success}</Alert> : ""}
            <Button color="primary" onClick={submitCategory}>
              Submit
            </Button>
          </Form>
        </CardBody>
      </Card>
    </Col>
  );
};
const mapStateToProps = state => ({
  login: state.login,
  category: state.category
});
export default connect(mapStateToProps, { addCategory, clearCategoryError })(
  AddCategory
);
