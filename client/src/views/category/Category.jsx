import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col,
  Button
} from "reactstrap";
import "components/Collapsible/Collapsible";
import { connect } from "react-redux";
import { MDBDataTable } from "mdbreact";
import AddCategory from "./AddCategory";
import AddSubCategory from "./AddSubCategory";
import {
  setCategorySuccess,
  getCategories,
  getCategory,
  getTotalCategory,
  getActiveCategory,
  getInActiveCategory
} from "../../actions/category/categoryActions";

class Category extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAddCat: false,
      isAddSub: false,
      total: 0,
      active: 0,
      inactive: 0,
      data: {
        columns: [
          {
            label: "Name",
            field: "name",
            sort: "asc"
          },
          {
            label: "Subcategory",
            field: "subcategory"
          },
          {
            label: "Status",
            field: "status"
          },
          {
            label: "Actions",
            field: "action"
          }
        ],
        rows: []
      }
    };
    this.toggleAddCategory = this.toggleAddCategory.bind(this);
    this.toggleAddSubCategory = this.toggleAddSubCategory.bind(this);
  }

  componentWillMount() {
    this.props.getCategories();
    this.props.getTotalCategory();
    this.props.getActiveCategory();
    this.props.getInActiveCategory();
  }

  componentDidCatch() {
    this.props.setCategorySuccess(false);
  }

  componentDidUpdate(prevProps, prevState) {
    window.onpopstate = e => {
      if (this.state.isAddCat || this.isAddSub) {
        e.preventDefault();
        this.props.history.go(1);
        this.props.setCategorySuccess(false);
        this.state({
          isAddCat: false,
          isAddSub: false
        });
      }
    };
    if (
      prevProps.category.total !== this.props.category.total ||
      prevProps.category.active !== this.props.category.active ||
      prevProps.category.inactive !== this.props.category.inactive
    ) {
      this.setState({
        total: this.props.category.total,
        active: this.props.category.active,
        inactive: this.props.category.inactive
      });
    }
    if (prevProps.category.categories !== this.props.category.categories) {
      let newRows = [...this.props.category.categories];
      let data = [];
      newRows.map(i => {
        let temp = {
          name: i.name,
          subcategory: (
            <div>
              {i.subcategory.length > 0 ? (
                <ol style={{ listStyleType: "numeric" }}>
                  {i.subcategory.map(function(item) {
                    return <li key={item}>{item.name}</li>;
                  })}
                </ol>
              ) : (
                <div>No subcategory added</div>
              )}
            </div>
          ),
          status: i.status ? "Active" : "Inactive",
          action: (
            <div className="text-center">
              <Button
                onClick={() => {
                  this.props.getCategory(i._id);
                  this.toggleAddSubCategory();
                }}
                className="btn-round"
                color="primary"
                outline
                size="sm"
              >
                View
              </Button>
            </div>
          )
        };
        data.push(temp);
      });
      this.setState({
        data: {
          ...this.state.data,
          rows: data
        }
      });
      this.props.setCategorySuccess(false);
    }
  }

  toggleAddCategory() {
    this.setState({
      isAddCat: !this.state.isAddCat
    });
  }

  toggleAddSubCategory(catData) {
    this.setState({
      isAddSub: !this.state.isAddSub,
      categoryData: catData
    });
  }
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col lg="4" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-globe text-warning" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Category</p>
                        <CardTitle tag="p">{this.state.total}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getTotalCategory}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="4" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-money-coins text-success" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Active</p>
                        <CardTitle tag="p">{this.state.active}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getActiveCategory}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="4" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-vector text-danger" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Inactive</p>
                        <CardTitle tag="p">{this.state.inactive}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getTotalCategory}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            {this.state.isAddCat ? (
              <AddCategory cancelCatToggle={() => this.toggleAddCategory()} />
            ) : this.state.isAddSub ? (
              <AddSubCategory
                categoryData={this.state.categoryData}
                cancelAddSubToggle={() => this.toggleAddSubCategory()}
              />
            ) : (
              <Col md="12">
                <Card>
                  <CardHeader>
                    <Row>
                      <Col md="3">
                        <CardTitle tag="h4">Category Details</CardTitle>
                      </Col>
                      <Col
                        mod="4"
                        className="text-right"
                        style={{ marginRight: 10 }}
                      >
                        <Button
                          className="btn-round"
                          color="primary"
                          type="submit"
                          outline
                          onClick={this.toggleAddCategory}
                        >
                          Add Category
                        </Button>
                      </Col>
                    </Row>
                  </CardHeader>
                  <CardBody>
                    <MDBDataTable
                      bordered
                      responsiveSm
                      data={this.state.data}
                    />
                  </CardBody>
                </Card>
              </Col>
            )}
          </Row>
        </div>
      </>
    );
  }
}
const mapStateToProps = state => ({
  login: state.login,
  category: state.category
});
export default connect(mapStateToProps, {
  setCategorySuccess,
  getCategory,
  getCategories,
  getTotalCategory,
  getActiveCategory,
  getInActiveCategory
})(Category);
