import React, { useState, useEffect } from "react";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Alert,
  Row,
  FormFeedback
} from "reactstrap";
import { Icon, Image } from "semantic-ui-react";
import { connect } from "react-redux";
import { addBrand, clearBrandError } from "../../actions/brand/brandActions";

const AddBrand = ({ addBrand, clearBrandError, cancelToggle, brand }) => {
  const [imagePreviewUrl, setimagePreviewUrl] = useState("");
  const [fileDetail, setFileDetail] = useState("");
  const [name, setName] = useState("");
  const [url, setUrl] = useState("");
  const [description, setDescription] = useState("");
  const [error, setError] = useState(null);
  const [success, setSuccessMessage] = useState("");

  const handleChangeName = event => {
    setName(event.target.value);
  };
  const handleChangeUrl = event => {
    setUrl(event.target.value);
  };

  const handleChangeDescription = event => {
    setDescription(event.target.value);
  };

  const uploadBrandImage = e => {
    e.preventDefault();
    setFileDetail(e.target.files[0]);
    let reader = new FileReader();
    console.log(e.target.files[0]);
    reader.onloadend = () => {
      setimagePreviewUrl(reader.result);
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  const submitBrand = e => {
    e.preventDefault();
    const data = {
      name: name,
      url: url,
      description: description
    };
    addBrand(fileDetail, data);
  };
  useEffect(() => {
    setError(brand.error);
  }, [brand.error]);
  useEffect(() => {
    clearBrandError();
  }, [clearBrandError]);
  useEffect(() => {
    if (brand.success) {
      setSuccessMessage("Added Successfully!");

      setTimeout(() => {
        setSuccessMessage("");
        setName("");
        setError("");
        setDescription("");
        setUrl("");
        setimagePreviewUrl("");
        setFileDetail("");
      }, 2000);
    }
  }, [brand.success, cancelToggle]);

  return (
    <Col md="12">
      <Card>
        <CardHeader>
          <Row>
            <Col md="3">
              <CardTitle tag="h4">Add New Brand</CardTitle>
            </Col>
            <Col mod="4" className="text-right" style={{ marginRight: 10 }}>
              <Button
                className="btn-round"
                color="danger"
                outline
                onClick={() => cancelToggle()}
              >
                Cancel
              </Button>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Form>
            <FormGroup>
              <Label for="brandName">Brand Name</Label>
              <Input
                type="text"
                name="brandName"
                id="brandName"
                placeholder="YSL Beauty"
                value={name}
                onChange={handleChangeName}
                invalid={error && error.brand ? true : false}
              />
              {error && error.brand ? (
                <FormFeedback invalid>{error.brand}</FormFeedback>
              ) : null}
            </FormGroup>
            <FormGroup>
              <Label for="brandUrl">Brand Website Url</Label>
              <Input
                type="text"
                name="brandUrl"
                id="brandUrl"
                placeholder="https://www.yslbeautyus.com/"
                value={url}
                onChange={handleChangeUrl}
              />
            </FormGroup>

            <div
              style={{
                borderRadius: "20%",
                alignItems: "center",
                justifyContent: "center",
                border: "1px solid #eee",
                display: "flex",
                textAlign: "center",
                marginBottom: "10",
                width: 150,
                height: 150
              }}
            >
              {!imagePreviewUrl ? (
                <div style={{ alignItems: "center" }}>
                  <Image
                    src="https://react.semantic-ui.com/images/wireframe/square-image.png"
                    size="small"
                  />
                </div>
              ) : (
                <img
                  style={{ borderRadius: "20%", width: 150, height: 150 }}
                  alt="..."
                  src={imagePreviewUrl}
                />
              )}
            </div>
            <div style={{ height: "10px" }} />
            <Col style={{ paddingLeft: "0", paddingTop: "10" }}>
              <Input
                style={{ marginTop: "10" }}
                type="file"
                accept="image/png, image/jpeg"
                id="brandLogo"
                onChange={uploadBrandImage}
              />
              <FormText color="muted">
                Click Upload button to upload the Logo of the brand.
              </FormText>
            </Col>
            <div style={{ height: "10px" }} />
            <FormGroup>
              <Label for="description">Brand Description</Label>
              <Input
                type="textarea"
                name="text"
                id="description"
                value={description}
                onChange={handleChangeDescription}
              />
            </FormGroup>
            {success !== "" ? <Alert color="success">{success}</Alert> : ""}
            <Button color="primary" onClick={submitBrand}>
              Submit
            </Button>
          </Form>
        </CardBody>
      </Card>
    </Col>
  );
};

const mapStateToProps = state => ({
  login: state.login,
  brand: state.brand
});
export default connect(mapStateToProps, { addBrand, clearBrandError })(
  AddBrand
);
