import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Table,
  Row,
  Col,
  Button
} from "reactstrap";
import { connect } from "react-redux";
import {
  setBrandSuccess,
  getBrands,
  getBrand,
  getActiveBrands,
  getInActiveBrands,
  getTotalBrands
} from "../../actions/brand/brandActions";
import { MDBDataTable } from "mdbreact";
import AddBrands from "./AddBrand";
import BrandDetail from "./BrandDetail";

class Brands extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAdd: false,
      isEdit: false,
      total: 0,
      active: 0,
      inactive: 0,
      data: {
        columns: [
          {
            label: "Name",
            field: "name",
            sort: "asc"
          },
          {
            label: "Url",
            field: "url"
          },
          {
            label: "Status",
            field: "status"
          },
          {
            label: "Actions",
            field: "action"
          }
        ],
        rows: []
      }
    };
    this.toggle = this.toggle.bind(this);
    this.toggleEdit = this.toggleEdit.bind(this);
  }
  toggle() {
    this.setState({
      isAdd: !this.state.isAdd
    });
  }

  toggleEdit() {
    this.setState({
      isEdit: !this.state.isEdit
    });
  }
  componentWillMount() {
    this.props.getBrands();
    this.props.getActiveBrands();
    this.props.getInActiveBrands();
    this.props.getTotalBrands();
  }
  componentDidMount() {
    this.props.setBrandSuccess(false);
  }
  componentDidUpdate(prevProps, prevState) {
    window.onpopstate = e => {
      if (this.state.isAdd || this.state.isEdit) {
        e.preventDefault();
        this.props.history.go(1);
        this.props.setBrandSuccess(false);
        this.setState({
          isAdd: false,
          isEdit: false
        });
      }
    };
    if (
      prevProps.brand.total !== this.props.brand.total ||
      prevProps.brand.active !== this.props.brand.active ||
      prevProps.brand.inactive !== this.props.brand.inactive
    ) {
      this.setState({
        total: this.props.brand.total,
        active: this.props.brand.active,
        inactive: this.props.brand.inactive
      });
    }
    if (prevProps.brand.brands !== this.props.brand.brands) {
      let newRows = [...this.props.brand.brands];
      let data = [];
      newRows.map(i => {
        let temp = {
          name: i.name,
          url: i.url,
          _id: i._id,
          active: i.active,
          status: i.active ? "Active" : "Inactive",
          action: (
            <div className="text-center">
              <Button
                onClick={() => {
                  this.props.getBrand(i._id);
                  this.toggleEdit();
                }}
                className="btn-round"
                color="primary"
                outline
                size="sm"
              >
                View
              </Button>
            </div>
          )
        };
        data.push(temp);
      });

      this.setState({
        data: {
          ...this.state.data,
          rows: data
        }
      });
      this.props.setBrandSuccess(false);
    }
  }

  render() {
    console.log(this.state);
    return (
      <>
        <div className="content">
          <Row>
            <Col lg="4" md="7" sm="7">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-globe text-warning" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Total Brands</p>
                        <CardTitle tag="p">{this.state.total}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getTotalBrands}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="4" md="7" sm="7">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-money-coins text-success" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Active</p>
                        <CardTitle tag="p">{this.state.active}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getActiveBrands}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="4" md="7" sm="7">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-vector text-danger" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Inactive</p>
                        <CardTitle tag="p">{this.state.inactive}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getInActiveBrands}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            {this.state.isAdd ? (
              <Col>
                <AddBrands cancelToggle={() => this.toggle()} />
              </Col>
            ) : this.state.isEdit ? (
              <Col>
                <BrandDetail cancelToggle={() => this.toggleEdit()} />
              </Col>
            ) : (
              <Col md="12">
                <Card>
                  <CardHeader>
                    <Row>
                      <Col md="3">
                        <CardTitle tag="h4">Brand Details</CardTitle>
                      </Col>
                      <Col
                        mod="4"
                        className="text-right"
                        style={{ marginRight: 10 }}
                      >
                        <Button
                          className="btn-round"
                          color="primary"
                          outline
                          onClick={this.toggle}
                        >
                          Add Brand
                        </Button>
                      </Col>
                    </Row>
                  </CardHeader>
                  <CardBody>
                    <MDBDataTable
                      bordered
                      responsiveSm
                      data={this.state.data}
                    />
                  </CardBody>
                </Card>
              </Col>
            )}
          </Row>
        </div>
      </>
    );
  }
}
const mapStateToProps = state => ({
  login: state.login,
  brand: state.brand
});
export default connect(mapStateToProps, {
  setBrandSuccess,
  getBrands,
  getBrand,
  getActiveBrands,
  getInActiveBrands,
  getTotalBrands
})(Brands);
