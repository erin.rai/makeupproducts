import React, { useState, useEffect } from "react";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Alert,
  Row,
  FormFeedback
} from "reactstrap";
import { Icon, Image } from "semantic-ui-react";
import { connect } from "react-redux";
import {
  addVendorData,
  clearVendorError
} from "../../actions/vendor/vendorActions";

const AddVendor = ({ addVendorData, clearVendorError, cancelToggle, ven }) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [url, setUrl] = useState("");
  const [address, setAddress] = useState("");
  const [error, setError] = useState(null);
  const [success, setSuccessMessage] = useState("");

  const handleChangeName = event => {
    setName(event.target.value);
  };
  const handleChangeUrl = event => {
    setUrl(event.target.value);
  };

  const handleChangeEmail = event => {
    setEmail(event.target.value);
  };

  const handleChangePhone = event => {
    setPhone(event.target.value);
  };

  const hanldleChangeAddress = event => {
    setAddress(event.target.value);
  };

  const submitBrand = e => {
    e.preventDefault();
    const data = {
      name: name,
      email: email,
      phone: phone,
      address: address,
      url: url
    };
    addVendorData(data);
  };
  useEffect(() => {
    setError(ven.error);
  }, [ven.error]);
  useEffect(() => {
    clearVendorError();
  }, [clearVendorError]);
  useEffect(() => {
    if (ven.success) {
      setSuccessMessage("Vendor Added Successfully!");

      setTimeout(() => {
        setSuccessMessage("");
        setName("");
        setError("");
        setAddress("");
        setUrl("");
        setPhone("");
        setEmail("");
      }, 2000);
    }
  }, [ven.success, cancelToggle]);

  return (
    <Col md="12">
      <Card>
        <CardHeader>
          <Row>
            <Col md="3">
              <CardTitle tag="h4">Add New Vendor</CardTitle>
            </Col>
            <Col mod="4" className="text-right" style={{ marginRight: 10 }}>
              <Button
                className="btn-round"
                color="danger"
                outline
                onClick={() => cancelToggle()}
              >
                Cancel
              </Button>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Form>
            <FormGroup>
              <Label for="vendorName">Vendor Name</Label>
              <Input
                type="text"
                name="vendorName"
                id="vendorName"
                placeholder="YSL Beauty"
                value={name}
                onChange={handleChangeName}
                invalid={error && error.name ? true : false}
              />
              {error && error.name ? (
                <FormFeedback invalid>{error.name}</FormFeedback>
              ) : null}
            </FormGroup>
            <FormGroup>
              <Label for="vendorEmail">Email</Label>
              <Input
                type="email"
                name="email"
                id="email"
                placeholder="example@gmail.com"
                value={email}
                onChange={handleChangeEmail}
                invalid={error && error.email ? true : false}
              />
              {error && error.email ? (
                <FormFeedback invalid>{error.email}</FormFeedback>
              ) : null}
            </FormGroup>
            <FormGroup>
              <Label for="vendorPhone">Phone</Label>
              <Input
                type="text"
                name="phone"
                id="phone"
                placeholder="+9779808062322"
                value={phone}
                onChange={handleChangePhone}
                invalid={error && error.phone ? true : false}
              />
              {error && error.phone ? (
                <FormFeedback invalid>{error.phone}</FormFeedback>
              ) : null}
            </FormGroup>
            <FormGroup>
              <Label for="vendorUrl">Vendor Website Url</Label>
              <Input
                type="text"
                name="vendorUrl"
                id="vendorUrl"
                placeholder="https://www.yslbeautyus.com/"
                value={url}
                onChange={handleChangeUrl}
              />
            </FormGroup>
            <div style={{ height: "10px" }} />
            <FormGroup>
              <Label for="address">Address</Label>
              <Input
                type="text"
                name="address"
                id="address"
                value={address}
                onChange={hanldleChangeAddress}
              />
            </FormGroup>
            {success !== "" ? <Alert color="success">{success}</Alert> : ""}
            <Button color="primary" onClick={submitBrand}>
              Submit
            </Button>
          </Form>
        </CardBody>
      </Card>
    </Col>
  );
};

const mapStateToProps = state => ({
  login: state.login,
  ven: state.vendor
});
export default connect(mapStateToProps, { addVendorData, clearVendorError })(
  AddVendor
);
