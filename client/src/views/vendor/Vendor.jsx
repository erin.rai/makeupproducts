import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Table,
  Row,
  Col,
  Button
} from "reactstrap";
import { connect } from "react-redux";
import {
  setVendorSuccess,
  getVendors,
  getVendor,
  getActiveVendors,
  getInActiveVendors,
  getTotalVendor
} from "../../actions/vendor/vendorActions";
import { MDBDataTable } from "mdbreact";
import AddVendor from "./AddVendor";
import VendorDetail from "./VendorDetail";

class Vendor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAdd: false,
      isEdit: false,
      total: 0,
      active: 0,
      inactive: 0,
      data: {
        columns: [
          {
            label: "Name",
            field: "name",
            sort: "asc"
          },
          {
            label: "Email",
            field: "email",
            sort: "asc"
          },
          {
            label: "Phone",
            field: "phone",
            sort: "asc"
          },
          {
            label: "Address",
            field: "address"
          },
          {
            label: "Status",
            field: "status"
          },
          {
            label: "Actions",
            field: "action"
          }
        ],
        rows: []
      }
    };
    this.toggle = this.toggle.bind(this);
    this.toggleEdit = this.toggleEdit.bind(this);
  }
  toggle() {
    this.setState({
      isAdd: !this.state.isAdd
    });
  }

  toggleEdit() {
    this.setState({
      isEdit: !this.state.isEdit
    });
  }
  componentWillMount() {
    this.props.getVendors();
    this.props.getActiveVendors();
    this.props.getInActiveVendors();
    this.props.getTotalVendor();
  }
  componentDidMount() {
    this.props.setVendorSuccess(false);
  }
  componentDidUpdate(prevProps, prevState) {
    window.onpopstate = e => {
      if (this.state.isAdd || this.state.isEdit) {
        e.preventDefault();
        this.props.history.go(1);
        this.props.setVendorSuccess(false);
        this.setState({
          isAdd: false,
          isEdit: false
        });
      }
    };
    if (
      prevProps.ven.total !== this.props.ven.total ||
      prevProps.ven.active !== this.props.ven.active ||
      prevProps.ven.inactive !== this.props.ven.inactive
    ) {
      this.setState({
        total: this.props.ven.total,
        active: this.props.ven.active,
        inactive: this.props.ven.inactive
      });
    }
    if (prevProps.ven.vendors !== this.props.ven.vendors) {
      console.log(this.props.ven.vendors);
      let newRows = [...this.props.ven.vendors];
      // newRows = newRows.slice(0, -1);
      let data = [];
      newRows.map(i => {
        let temp = {
          name: i.name,
          email: i.email,
          phone: i.phone,
          address: i.address,
          status: i.active ? "Active" : "Inactive",
          action: (
            <div className="text-center">
              <Button
                onClick={() => {
                  this.props.getVendor(i._id);
                  this.toggleEdit();
                }}
                className="btn-round"
                color="primary"
                outline
                size="sm"
              >
                View
              </Button>
            </div>
          )
        };
        data.push(temp);
      });

      this.setState({
        data: {
          ...this.state.data,
          rows: data
        }
      });
      this.props.setVendorSuccess(false);
    }
  }
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col lg="4" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-globe text-warning" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Vendor</p>
                        <CardTitle tag="p">{this.state.total}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getTotalVendor}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="4" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-money-coins text-success" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Active</p>
                        <CardTitle tag="p">{this.state.active}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getActiveVendors}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="4" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-vector text-danger" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Inactive</p>
                        <CardTitle tag="p">{this.state.inactive}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div
                    className="stats"
                    onClick={this.props.getInActiveVendors}
                  >
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            {this.state.isAdd ? (
              <Col>
                <AddVendor cancelToggle={() => this.toggle()} />
              </Col>
            ) : this.state.isEdit ? (
              <Col>
                <VendorDetail cancelToggle={() => this.toggleEdit()} />
              </Col>
            ) : (
              <Col md="12">
                <Card>
                  <CardHeader>
                    <Row>
                      <Col md="3">
                        <CardTitle tag="h4">Vendor Details</CardTitle>
                      </Col>
                      <Col
                        mod="4"
                        className="text-right"
                        style={{ marginRight: 10 }}
                      >
                        <Button
                          className="btn-round"
                          color="primary"
                          outline
                          onClick={this.toggle}
                        >
                          Add Vendor
                        </Button>
                      </Col>
                    </Row>
                  </CardHeader>
                  <CardBody>
                    <MDBDataTable
                      bordered
                      responsiveSm
                      data={this.state.data}
                    />
                  </CardBody>
                </Card>
              </Col>
            )}
          </Row>
        </div>
      </>
    );
  }
}
const mapStateToProps = state => ({
  login: state.login,
  ven: state.vendor
});
export default connect(mapStateToProps, {
  setVendorSuccess,
  getVendors,
  getVendor,
  getActiveVendors,
  getInActiveVendors,
  getTotalVendor
})(Vendor);
