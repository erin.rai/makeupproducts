import React from "react";
import { connect } from "react-redux";
import { MDBDataTable } from "mdbreact";
import moment from "moment";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col,
  Button
} from "reactstrap";
import { getStocks } from "../../actions/stock/stockActions";

class Stocks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        columns: [
          {
            label: "Product Name",
            field: "productName"
          },

          {
            label: "Quantity",
            field: "quantity"
          },
          {
            label: "Unit Price",
            field: "unitPrice"
          },
          {
            label: "Buy Price",
            field: "buyPrice"
          }
        ],
        rows: []
      }
    };
  }

  componentWillMount() {
    this.props.getStocks();
  }

  componentDidUpdate(prevPros, prevState) {
    if (prevPros.sto.stocks !== this.props.sto.stocks) {
      let newRows = [...this.props.sto.stocks];
      let data = [];
      console.log(newRows);
      newRows.map(i => {
        let temp = {
          productName: i.productId.name,
          quantity: i.quantity,
          unitPrice: i.unitPrice,
          buyPrice: i.buyPrice
        };
        data.push(temp);
      });
      this.setState({
        data: {
          ...this.state.data,
          rows: data
        }
      });
    }
  }

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-globe text-warning" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Purchase</p>
                        <CardTitle tag="p">150</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="fas fa-sync-alt" /> Update Now
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-money-coins text-success" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Active</p>
                        <CardTitle tag="p">22</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="far fa-calendar" /> Monthly
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-vector text-danger" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Inactive</p>
                        <CardTitle tag="p">23</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="far fa-clock" /> Monthly
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-favourite-28 text-primary" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Registered</p>
                        <CardTitle tag="p">35</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="fas fa-sync-alt" /> Monthly
                  </div>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <Row>
                    <Col md="3">
                      <CardTitle tag="h4">Purchases</CardTitle>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <MDBDataTable bordered responsiveSm data={this.state.data} />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  login: state.login,
  sto: state.stock
});

export default connect(mapStateToProps, {
  getStocks
})(Stocks);
