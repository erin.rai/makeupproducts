import React, { useState, useEffect, useRef } from "react";
import "rc-color-picker/assets/index.css";
import { Panel as ColorPickerPanel } from "rc-color-picker";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Row
} from "reactstrap";
import { connect } from "react-redux";
import {
  updatePurchase,
  clearPurchaseError,
  getProductList,
  getVendorList
} from "../../actions/purchase/purchaseActions";

const ViewPurchase = ({
  updatePurchase,
  clearPurchaseError,
  getProductList,
  getVendorList,
  cancelToggle,
  pur
}) => {
  const [vendor, setVendor] = useState("");
  const [vendorList, setVendorList] = useState([]);
  const [productList, setProductList] = useState([]);
  const [data, setData] = useState([]);
  const [paidStatus, setPaidStatus] = useState("");
  const [discount, setDiscount] = useState(0);
  const [tax, setTax] = useState(0);
  const [total, setTotal] = useState(0);
  const [subTotal, setSubTotal] = useState(0);
  const [success, setSuccessMessage] = useState("");
  const [error, setErrorMessage] = useState("");

  // useEffect(() => {
  //   if (pur.success) {
  //     setSuccessMessage("Purchase Added Successfully!");
  //     setTimeout(() => {
  //       setSuccessMessage("");
  //       setData([]);
  //       setErrorMessage("");
  //       setPaidStatus("paid");
  //       setDiscount(0);
  //       setVendor(vendorList[0].name);
  //       setTotal(0);
  //       setSubTotal(0);
  //       setTax(0);
  //     }, 2000);
  //   }
  // }, [pur.success, vendorList]);

  useEffect(() => {
    getProductList();
    getVendorList();
  }, [getProductList, getVendorList]);

  useEffect(() => {
    if (pur.purchase && vendorList.length > 0 && productList.length > 0) {
      let temp = [];
      pur.purchase.products.map(i => {
        var prod = productList.filter(function(item) {
          return item._id === i._id;
        });
        let obj = {
          name: prod[0].name,
          quantity: i.quantity,
          buyPrice: i.buyPrice,
          sellPrice: i.sellPrice,
          total: i.quantity * i.buyPrice
        };
        temp.push(obj);
      });
      setData(temp);
      var ven = vendorList.filter(function(item) {
        return item._id === pur.purchase.vendorId;
      });
      setVendor(ven[0].name);
      setDiscount(pur.purchase.discount);
      setTax(pur.purchase.tax);
      setPaidStatus(pur.purchase.paid);
      setTotal(pur.purchase.total);
    }
  }, [pur.purchase, productList, vendorList]);
  useEffect(() => {
    setProductList(pur.products);
    setVendorList(pur.vendors);
  }, [pur.products, pur.vendors]);

  const submitPurchase = () => {
    updatePurchase(pur.purchase._id);
  };
  const renderTableBody = data.map((value, i) => {
    return (
      <tr key={i}>
        <td>{value.name}</td>
        <td>{value.quantity}</td>
        <td>{value.buyPrice}</td>
        <td>{value.sellPrice}</td>
        <td>{value.total}</td>
      </tr>
    );
  });

  return (
    <Col md="12">
      <Card>
        <CardHeader>
          <Row>
            <Col md="5" lg="5">
              <CardTitle tag="h4">Add New Purchase</CardTitle>
            </Col>
            <Col md="3" lg="3"></Col>
            <Col
              md="4"
              lg="4"
              className="text-right"
              // style={{ marginRight: 10 }}
            >
              <Button
                className="btn-round"
                color="danger"
                outline
                onClick={() => cancelToggle()}
              >
                Cancel
              </Button>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Form>
            <Row>
              <Col md="3">
                <FormGroup>
                  <Label for="vendor">Vendor Name</Label>
                  <Input
                    type="text"
                    style={{
                      height: "30px",
                      paddingLeft: "5px",
                      paddingTop: "0px",
                      paddingRight: "0px",
                      paddingBottom: "0px"
                    }}
                    name="selectVendor"
                    id="selectVendor"
                    value={vendor}
                    disabled={true}
                  ></Input>
                </FormGroup>
              </Col>
              <Col md="6"></Col>
              <Col md="3">
                <FormGroup>
                  <Label for="status">Paid Status</Label>
                  <Input
                    type="text"
                    style={{
                      height: "30px",
                      paddingLeft: "5px",
                      paddingTop: "0px",
                      paddingRight: "0px",
                      paddingBottom: "0px"
                    }}
                    name="selectVendor"
                    id="selectVendor"
                    value={paidStatus ? "Paid" : "Unpaid"}
                    disabled={true}
                  ></Input>
                </FormGroup>
              </Col>
            </Row>
            <MDBTable responsiveSm>
              <MDBTableHead>
                <tr>
                  <th style={{ textTransform: "capitalize", width: "200px" }}>
                    Product Name
                  </th>
                  <th style={{ textTransform: "capitalize" }}>Quantity</th>
                  <th style={{ textTransform: "capitalize" }}>Buy Price</th>
                  <th style={{ textTransform: "capitalize" }}>Sell Price</th>
                  <th style={{ textTransform: "capitalize" }}>Total</th>
                </tr>
              </MDBTableHead>
              <MDBTableBody>{renderTableBody}</MDBTableBody>
            </MDBTable>
            <div
              style={{
                width: "100%",
                height: "2px",
                backgroundColor: "#dbdbdb"
              }}
            ></div>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}
            >
              <div style={{ paddingRight: "10px", fontSize: "12px" }}>
                Sub Total:
              </div>
              <div style={{ fontSize: "12px" }}>Rs. {subTotal}</div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}
            >
              <div style={{ paddingRight: "10px", fontSize: "12px" }}>
                Discount:
              </div>
              <div style={{ fontSize: "12px" }}>Rs. {discount}</div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}
            >
              <div style={{ paddingRight: "10px", fontSize: "12px" }}>Tax:</div>
              <div style={{ fontSize: "12px" }}>Rs. {tax.toString()}</div>
            </div>

            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}
            >
              <div style={{ paddingRight: "10px", fontSize: "12px" }}>
                Total:
              </div>
              <div style={{ fontSize: "12px" }}>Rs. {total}</div>
            </div>

            <div style={{ height: "10px" }} />
            {success !== "" ? <Alert color="success">{success}</Alert> : ""}
            {error !== "" ? <Alert color="danger">{error}</Alert> : ""}
            <Col className="text-right" style={{ paddingRight: "0px" }}>
              {paidStatus ? (
                <Button
                  style={{ width: "200px" }}
                  color="primary"
                  // onClick={submitPurchase}
                  disabled={true}
                >
                  Purchased
                </Button>
              ) : (
                <Button
                  style={{ width: "200px" }}
                  color="primary"
                  onClick={submitPurchase}
                  // disabled={true}
                >
                  Pay
                </Button>
              )}
            </Col>
          </Form>
        </CardBody>
      </Card>
    </Col>
  );
};

const mapStateToProps = state => ({
  login: state.login,
  pur: state.purchase
});

export default connect(mapStateToProps, {
  updatePurchase,
  clearPurchaseError,
  getProductList,
  getVendorList
})(ViewPurchase);
