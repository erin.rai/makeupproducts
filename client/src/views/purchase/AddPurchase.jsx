import React, { useState, useEffect, useRef } from "react";
import "rc-color-picker/assets/index.css";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Row
} from "reactstrap";
import { connect } from "react-redux";
import {
  addPurchase,
  clearPurchaseError,
  getProductList,
  getVendorList
} from "../../actions/purchase/purchaseActions";
import { setProduct } from "actions/product/productActions";

const AddPurchase = ({
  addPurchase,
  clearPurchaseError,
  getProductList,
  getVendorList,
  cancelToggle,
  pur
}) => {
  const [productName, setProductName] = useState("first");
  const [quantity, setQuantity] = useState(0);
  const [sellPrice, setSellPrice] = useState(0);
  const [buyPrice, setBuyPrice] = useState(0);
  const [vendor, setVendor] = useState("");
  const [vendorList, setVendorList] = useState([]);
  const [productList, setProductList] = useState([]);
  const [data, setData] = useState([]);
  const [paidStatus, setPaidStatus] = useState("paid");
  const [discountPercent, setDiscountPercent] = useState(0);
  const [taxPercent, setTaxPercent] = useState(0);
  const [discount, setDiscount] = useState(0);
  const [tax, setTax] = useState(0);
  const [indTotal, setIndTotal] = useState(0);
  const [total, setTotal] = useState(0);
  const [subTotal, setSubTotal] = useState(0);
  const [success, setSuccessMessage] = useState("");
  const [error, setErrorMessage] = useState("");

  useEffect(() => {
    if (pur.success) {
      setSuccessMessage("Purchase Added Successfully!");
      setTimeout(() => {
        setSuccessMessage("");
        setData([]);
        setErrorMessage("");
        setPaidStatus("paid");
        setDiscount(0);
        setVendor(vendorList[0].name);
        setTotal(0);
        setSubTotal(0);
        setTax(0);
        setDiscountPercent(0);
        setTaxPercent(0);
      }, 2000);
    }
  }, [pur.success, vendorList]);

  useEffect(() => {
    getProductList();
    getVendorList();
  }, [getProductList, getVendorList]);

  useEffect(() => {
    setProductList(pur.products);
    setVendorList(pur.vendors);
    setProductName(
      pur.products.length > 0 ? pur.products[0].name : "No Products"
    );
    setVendor(pur.vendors.length > 0 ? pur.vendors[0].name : "No Vendors");
  }, [pur.products, pur.vendors]);
  useEffect(() => {
    let sum = 0;
    data.map(i => {
      console.log(i);
      sum = i.total + sum;
    });
    if (sum === 0) {
      setSubTotal(0);
      setTotal(0);
      setDiscount(0);
      setTax(0);
    } else {
      console.log(sum);
      setSubTotal(sum.toFixed(2));
      let total = sum;
      total = sum - (discountPercent / 100) * sum;
      setDiscount(((discountPercent / 100) * sum).toFixed(2));
      setTax(((taxPercent / 100) * total).toFixed(2));
      total = total + (taxPercent / 100) * total;
      setTotal(total.toFixed(2));
    }
  }, [data, taxPercent, discountPercent]);
  const changeQuantity = e => {
    if (e.target.value >= 0) {
      setIndTotal(e.target.value * buyPrice);
      setQuantity(e.target.value);
    }
  };

  const changeDiscountPer = e => {
    if (e.target.value >= 0) {
      setDiscountPercent(e.target.value);
    }
  };
  const changeTaxPer = e => {
    if (e.target.value >= 0) {
      setTaxPercent(e.target.value);
    }
  };
  const changeProductName = (i, e) => {
    const temp = [...data];
    temp[i].productName = e.target.value;
    setData(temp);
  };

  const editChangeQuanity = (i, e) => {
    const temp = [...data];
    if (e.target.value >= 0) {
      temp[i].quantity = e.target.value;
      temp[i].total = e.target.value * temp[i].buyPrice;
      setData(temp);
    }
  };

  const editBuyPrice = (i, e) => {
    const temp = [...data];
    if (e.target.value >= 0) {
      temp[i].buyPrice = e.target.value;
      temp[i].total = e.target.value * temp[i].quantity;
      setData(temp);
    }
  };
  const editSellPrice = (i, e) => {
    const temp = [...data];
    if (e.target.value >= 0) {
      temp[i].sellPrice = e.target.value;

      setData(temp);
    }
  };
  const changeBuyPrice = e => {
    if (e.target.value >= 0) {
      setIndTotal(e.target.value * quantity);
      setBuyPrice(e.target.value);
    }
  };
  const changeSellPrice = e => {
    if (e.target.value >= 0) {
      setSellPrice(e.target.value);
    }
  };

  const editDataSubmit = i => {
    const temp = [...data];
    temp[i].isEdit = false;
    setData(temp);
  };

  const addData = e => {
    // if (productName === "No Products") {
    //   setErrorMessage("Product is required!");
    // } else {
    const temp = [...data];
    let newData = {
      productName: productName,
      quantity: quantity,
      buyPrice: buyPrice,
      sellPrice: sellPrice,
      isEdit: false,
      total: indTotal
    };
    temp.push(newData);
    setData(temp);
    setProductName(
      productList.length > 0 ? productList[0].name : "No Products"
    );
    setQuantity(0);
    setBuyPrice(0);
    setSellPrice(0);
    setIndTotal(0);
    // }
  };

  const editData = i => {
    const temp = [...data];
    temp[i].isEdit = true;
    setData(temp);
  };
  const removeData = i => {
    const temp = [...data];
    temp.splice(i, 1);
    setData(temp);
  };

  function renderInputs() {
    return (
      <tr>
        <td>
          <Input
            type="select"
            style={{
              height: "30px",
              paddingLeft: "5px",
              paddingTop: "0px",
              paddingRight: "5px",
              paddingBottom: "0px"
            }}
            name="selectBrand"
            id="selectBrand"
            value={productName}
            onChange={event => {
              setProductName(event.target.value);
            }}
          >
            {renderProductOption}
          </Input>
        </td>
        <td>
          <Input
            style={{
              height: "30px",
              paddingLeft: "5px",
              paddingRight: "5px",
              paddingTop: "0px",
              paddingBottom: "0px"
            }}
            type="number"
            name="quantity"
            id="quantity"
            placeholder="0"
            value={quantity}
            onChange={changeQuantity}
          />
        </td>
        <td>
          <Input
            style={{
              height: "30px",
              paddingLeft: "5px",
              paddingRight: "5px",
              paddingTop: "0px",
              paddingBottom: "0px"
            }}
            type="number"
            name="buyPrice"
            id="buyPrice"
            placeholder="0"
            value={buyPrice}
            onChange={changeBuyPrice}
          />
        </td>
        <td>
          <Input
            style={{
              height: "30px",
              paddingLeft: "5px",
              paddingRight: "5px",

              paddingTop: "0px",
              paddingBottom: "0px"
            }}
            type="number"
            name="sellPrice"
            id="sellPrice"
            placeholder="0"
            value={sellPrice}
            onChange={changeSellPrice}
          />
        </td>
        <td>{indTotal}</td>
        <td>
          <Col className="text-right" md="4" xs="4">
            <Button
              className="btn-icon"
              color="success"
              outline
              size="sm"
              onClick={addData}
            >
              <i className="fa fa-plus" />
            </Button>
          </Col>
        </td>
      </tr>
    );
  }
  const renderProductOption =
    productList.length > 0 ? (
      productList.map((value, i) => {
        return <option value={value.name}>{value.name}</option>;
      })
    ) : (
      <option>No Products</option>
    );
  const renderVendorOption =
    vendorList.length > 0 ? (
      vendorList.map((value, i) => {
        return <option value={value.name}>{value.name}</option>;
      })
    ) : (
      <option>No Vendors</option>
    );

  const submitPurchase = () => {
    setErrorMessage("");
    if (vendor === "No Vendors") {
      setErrorMessage("No Vendor Selected!");
    } else if (data.length > 0) {
      const finalProduct = [];
      data.map(i => {
        var prod = productList.filter(function(item) {
          return item.name === i.productName;
        });
        let id = prod[0]._id;
        let temp = {
          _id: id,
          quantity: i.quantity,
          sellPrice: i.sellPrice,
          buyPrice: i.buyPrice
        };
        finalProduct.push(temp);
      });
      var ven = vendorList.filter(function(item) {
        return item.name === vendor;
      });
      const purchaseData = {
        products: finalProduct,
        total: total,
        tax: tax,
        discount: discount,
        vendorId: ven[0]._id,
        paid: paidStatus === "paid" ? true : false
      };
      addPurchase(purchaseData);
    } else {
      setErrorMessage("Please Add At least one Product!");
    }
  };
  const renderTableBody = data.map((value, i) => {
    return value.isEdit ? (
      <tr key={i}>
        <td>
          <Input
            type="select"
            style={{
              height: "30px",
              paddingLeft: "5px",
              paddingTop: "0px",
              paddingRight: "5px",
              paddingBottom: "0px"
            }}
            name="selectBrand"
            id="selectBrand"
            value={value.productName}
            onChange={e => {
              changeProductName(i, e);
            }}
          >
            {renderProductOption}
          </Input>
        </td>
        <td>
          <Input
            style={{
              height: "30px",
              paddingLeft: "5px",
              paddingRight: "5px",
              paddingTop: "0px",
              paddingBottom: "0px"
            }}
            type="number"
            name="quantity"
            id="quantity"
            placeholder="0"
            value={value.quantity}
            onChange={e => editChangeQuanity(i, e)}
          />
        </td>
        <td>
          <Input
            style={{
              height: "30px",
              paddingLeft: "5px",
              paddingRight: "5px",
              paddingTop: "0px",
              paddingBottom: "0px"
            }}
            type="number"
            name="buyPrice"
            id="buyPrice"
            placeholder="0"
            value={value.buyPrice}
            onChange={e => {
              editBuyPrice(i, e);
            }}
          />
        </td>
        <td>
          <Input
            style={{
              height: "30px",
              paddingLeft: "5px",
              paddingRight: "5px",

              paddingTop: "0px",
              paddingBottom: "0px"
            }}
            type="number"
            name="sellPrice"
            id="sellPrice"
            placeholder="0"
            value={value.sellPrice}
            onChange={e => editSellPrice(i, e)}
          />
        </td>
        <td>{value.total}</td>
        <td>
          <Col className="text-right" md="4" xs="4">
            <Button
              className="btn-icon"
              color="success"
              outline
              size="sm"
              onClick={() => editDataSubmit(i)}
            >
              <i className="fa fa-check" />
            </Button>
          </Col>
        </td>
      </tr>
    ) : (
      <tr key={i}>
        <td>{value.productName}</td>
        <td>{value.quantity}</td>
        <td>{value.buyPrice}</td>
        <td>{value.sellPrice}</td>
        <td>{value.total}</td>
        <td>
          <Row>
            <Col className="text-right" md="1" xs="1">
              <Button
                className="btn-icon"
                color="danger"
                outline
                size="sm"
                onClick={() => {
                  removeData(i);
                }}
              >
                <i className="fa fa-trash" />
              </Button>
            </Col>
            <Col className="text-right" md="1" xs="1">
              <Button
                className="btn-icon"
                color="success"
                outline
                size="sm"
                onClick={() => editData(i)}
              >
                <i className="fa fa-edit" />
              </Button>
            </Col>
            <Col className="text-right" md="1" xs="1">
              <Button
                className="btn-icon"
                color="primary"
                outline
                size="sm"
                onClick={addData}
              >
                <i className="fa fa-plus" />
              </Button>
            </Col>
          </Row>
        </td>
      </tr>
    );
  });

  return (
    <Col md="12">
      <Card>
        <CardHeader>
          <Row>
            <Col md="5" lg="5">
              <CardTitle tag="h4">Add New Purchase</CardTitle>
            </Col>
            <Col md="3" lg="3"></Col>
            <Col
              md="4"
              lg="4"
              className="text-right"
              // style={{ marginRight: 10 }}
            >
              <Button
                className="btn-round"
                color="danger"
                outline
                onClick={() => cancelToggle()}
              >
                Cancel
              </Button>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Form>
            <Row>
              <Col md="3">
                <FormGroup>
                  <Label for="vendor">Select Vendor</Label>
                  <Input
                    type="select"
                    style={{
                      // height: "30px",
                      paddingLeft: "5px",
                      paddingTop: "0px",
                      paddingRight: "0px",
                      paddingBottom: "0px"
                    }}
                    name="selectVendor"
                    id="selectVendor"
                    value={vendor}
                    onChange={event => {
                      setVendor(event.target.value);
                    }}
                  >
                    {renderVendorOption}
                  </Input>
                </FormGroup>
              </Col>
              <Col md="3">
                <Label for="discPer">Discount Percent</Label>
                <Input
                  style={{
                    height: "30px",
                    paddingLeft: "5px",
                    paddingRight: "5px",

                    paddingTop: "0px",
                    paddingBottom: "0px"
                  }}
                  type="number"
                  name="discPer"
                  id="discPer"
                  placeholder="0"
                  value={discountPercent}
                  onChange={changeDiscountPer}
                />
              </Col>
              <Col md="3">
                <Label for="taxPer">Tax Percent</Label>
                <Input
                  style={{
                    height: "30px",
                    paddingLeft: "5px",
                    paddingRight: "5px",

                    paddingTop: "0px",
                    paddingBottom: "0px"
                  }}
                  type="number"
                  name="taxPer"
                  id="taxPer"
                  placeholder="0"
                  value={taxPercent}
                  onChange={changeTaxPer}
                />
              </Col>
              <Col className="text-right" md="3">
                <FormGroup>
                  <Label for="status">Status</Label>
                  <Input
                    type="select"
                    style={{
                      // height: "30px",
                      paddingLeft: "5px",
                      paddingTop: "0px",
                      paddingRight: "0px",
                      paddingBottom: "0px"
                    }}
                    name="status"
                    id="status"
                    value={paidStatus}
                    onChange={event => {
                      setPaidStatus(event.target.value);
                    }}
                  >
                    <option value={"paid"}>paid</option>
                    <option value={"unpaid"}>unpaid</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <MDBTable responsiveSm>
              <MDBTableHead>
                <tr>
                  <th style={{ textTransform: "capitalize", width: "200px" }}>
                    Product Name
                  </th>
                  <th style={{ textTransform: "capitalize" }}>Quantity</th>
                  <th style={{ textTransform: "capitalize" }}>Buy Price</th>
                  <th style={{ textTransform: "capitalize" }}>Sell Price</th>
                  <th style={{ textTransform: "capitalize" }}>Total</th>
                  <th style={{ textTransform: "capitalize" }}>Actions</th>
                </tr>
              </MDBTableHead>
              <MDBTableBody>
                {renderTableBody}
                {renderInputs()}
              </MDBTableBody>
            </MDBTable>
            <div
              style={{
                width: "100%",
                height: "2px",
                backgroundColor: "#dbdbdb"
              }}
            ></div>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}
            >
              <div style={{ paddingRight: "10px", fontSize: "12px" }}>
                Sub Total:
              </div>
              <div style={{ fontSize: "12px" }}>Rs. {subTotal}</div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}
            >
              <div style={{ paddingRight: "10px", fontSize: "12px" }}>
                Discount:
              </div>
              <div style={{ fontSize: "12px" }}>Rs. {discount}</div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}
            >
              <div style={{ paddingRight: "10px", fontSize: "12px" }}>Tax:</div>
              <div style={{ fontSize: "12px" }}>Rs. {tax.toString()}</div>
            </div>

            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}
            >
              <div style={{ paddingRight: "10px", fontSize: "12px" }}>
                Total:
              </div>
              <div style={{ fontSize: "12px" }}>Rs. {total}</div>
            </div>

            <div style={{ height: "10px" }} />
            {success !== "" ? <Alert color="success">{success}</Alert> : ""}
            {error !== "" ? <Alert color="danger">{error}</Alert> : ""}
            <Col className="text-right" style={{ paddingRight: "0px" }}>
              <Button
                style={{ width: "200px" }}
                color="primary"
                onClick={submitPurchase}
              >
                Save
              </Button>
            </Col>
          </Form>
        </CardBody>
      </Card>
    </Col>
  );
};

const mapStateToProps = state => ({
  login: state.login,
  pur: state.purchase
});

export default connect(mapStateToProps, {
  addPurchase,
  clearPurchaseError,
  getProductList,
  getVendorList
})(AddPurchase);
