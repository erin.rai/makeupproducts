import React from "react";
import { connect } from "react-redux";
import { MDBDataTable } from "mdbreact";
import moment from "moment";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col,
  Button
} from "reactstrap";
import AddPurchase from "./AddPurchase";
import {
  setPurchaseSuccess,
  getPurchase,
  getPurchases,
  getDailyPurchase,
  getWeelyPurchase,
  getMonthlyPurchase,
  getTotalUnpaidPurchase
} from "../../actions/purchase/purchaseActions";
import ViewPurchase from "./ViewPurchase";
// import EditProduct from "./EditProduct";

class Purchases extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAdd: false,
      isEdit: false,
      daily: 0,
      weekly: 0,
      monthly: 0,
      totalUnpaid: 0,
      data: {
        columns: [
          {
            label: "Purchase Date",
            field: "purchaseDate"
          },
          {
            label: "No of Products",
            field: "products"
          },
          {
            label: "Vendor Name",
            field: "vendor"
          },
          {
            label: "Total",
            field: "total"
          },
          {
            label: "Paid",
            field: "paid"
          },
          {
            label: "Actions",
            field: "action"
          }
        ],
        rows: []
      }
    };
    this.toggle = this.toggle.bind(this);
    this.toggleEdit = this.toggleEdit.bind(this);
  }

  toggle() {
    this.setState({
      isAdd: !this.state.isAdd
    });
  }

  toggleEdit() {
    this.setState({
      isEdit: !this.state.isEdit
    });
  }

  componentWillMount() {
    this.props.getPurchases();
    this.props.getDailyPurchase();
    this.props.getWeelyPurchase();
    this.props.getMonthlyPurchase();
    this.props.getTotalUnpaidPurchase();
  }

  componentDidMount() {
    this.props.setPurchaseSuccess(false);
  }

  componentDidUpdate(prevProps, prevState) {
    window.onpopstate = e => {
      if (this.state.isAdd || this.state.isEdit) {
        e.preventDefault();
        this.props.history.go(1);
        this.setState({
          isAdd: false,
          isEdit: false
        });
      }
    };

    if (
      prevProps.pur.daily !== this.props.pur.daily ||
      prevProps.pur.weekly !== this.props.pur.weekly ||
      prevProps.pur.monthly !== this.props.pur.monthly ||
      prevProps.pur.unpaidCount !== this.props.pur.unpaidCount
    ) {
      this.setState({
        daily: this.props.pur.daily,
        monthly: this.props.pur.monthly,
        weekly: this.props.pur.weekly,
        totalUnpaid: this.props.pur.unpaidCount
      });
    }

    if (prevProps.pur.purchases !== this.props.pur.purchases) {
      let newRows = [...this.props.pur.purchases];
      let data = [];
      console.log(newRows);
      newRows.map(i => {
        let temp = {
          purchaseDate: moment(i.purchaseDate).format("YYYY-MM-DD"),
          products: i.products,
          vendor: i.vendor[0].name,
          total: i.total,
          paid: i.paid ? "paid" : "unpaid",
          action: (
            <div className="text-center">
              <Button
                onClick={() => {
                  this.props.getPurchase(i._id);
                  this.toggleEdit();
                }}
                className="btn-round"
                color="primary"
                outline
                size="sm"
              >
                View
              </Button>
            </div>
          )
        };
        data.push(temp);
      });
      this.setState({
        data: {
          ...this.state.data,
          rows: data
        }
      });
      this.props.setPurchaseSuccess(false);
    }
  }

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-globe text-warning" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Purchase</p>
                        <CardTitle tag="p">{this.state.daily}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="far fa-clock" /> Today
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-money-coins text-success" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Purchase</p>
                        <CardTitle tag="p">{this.state.weekly}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="far fa-calendar" /> Past 7 Days
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-vector text-danger" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Purchase</p>
                        <CardTitle tag="p">{this.state.monthly}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div
                    className="stats"
                    onClick={this.props.getTotalUnpaidPurchase}
                  >
                    <i className="far fa-calendar" /> Past 30 days
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-favourite-28 text-primary" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Unpaid</p>
                        <CardTitle tag="p">{this.state.totalUnpaid}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            {this.state.isAdd ? (
              <Col>
                <AddPurchase cancelToggle={() => this.toggle()} />
              </Col>
            ) : this.state.isEdit ? (
              <Col>
                <ViewPurchase
                  cancelToggle={() => this.toggleEdit()}
                ></ViewPurchase>
              </Col>
            ) : (
              <Col md="12">
                <Card>
                  <CardHeader>
                    <Row>
                      <Col md="3">
                        <CardTitle tag="h4">Purchases</CardTitle>
                      </Col>
                      <Col
                        mod="4"
                        className="text-right"
                        style={{ marginRight: 10 }}
                      >
                        <Button
                          className="btn-round"
                          color="primary"
                          type="submit"
                          outline
                          onClick={this.toggle}
                        >
                          Add Purchase
                        </Button>
                      </Col>
                    </Row>
                  </CardHeader>
                  <CardBody>
                    <MDBDataTable
                      bordered
                      responsiveSm
                      data={this.state.data}
                    />
                  </CardBody>
                </Card>
              </Col>
            )}
          </Row>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  login: state.login,
  pur: state.purchase
});

export default connect(mapStateToProps, {
  setPurchaseSuccess,
  getPurchases,
  getPurchase,
  getDailyPurchase,
  getWeelyPurchase,
  getMonthlyPurchase,
  getTotalUnpaidPurchase
})(Purchases);
