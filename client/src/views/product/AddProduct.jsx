import React, { useState, useEffect, useRef } from "react";
import "rc-color-picker/assets/index.css";
import { Panel as ColorPickerPanel } from "rc-color-picker";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Row,
  Alert
} from "reactstrap";
import { connect } from "react-redux";
import {
  addProduct,
  clearProductError,
  getBrandList,
  setProductSuccess,
  getCategoriesList
} from "../../actions/product/productActions";

const AddProduct = ({
  addProduct,
  clearProductError,
  getBrandList,
  setProductSuccess,
  getCategoriesList,
  cancelToggle,
  prod
}) => {
  const [color, setColor] = useState({ color: "#457899", alpha: 100 });
  const [colorsSet, setcolorsSet] = useState([]);
  const [file, setFile] = useState([]);
  const [name, setName] = useState("");
  const [model, setModel] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [brands, setBrands] = useState([]);
  const [brand, setBrand] = useState("");
  const [subCategory, setSubCategory] = useState("");
  const [images, setImages] = useState("");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const uploadMultipleFiles = e => {
    let fileObj = [];
    let imageArray = [];
    let fileArray = [];
    fileObj.push(e.target.files);
    console.log(fileObj);
    for (let i = 0; i < fileObj[0].length; i++) {
      imageArray.push(fileObj[0][i]);
    }
    setImages(imageArray);
    fileObj.push(e.target.files);
    for (let i = 0; i < fileObj[0].length; i++) {
      fileArray.push(URL.createObjectURL(fileObj[0][i]));
    }
    setFile(fileArray);
  };

  const changeColor = obj => {
    setColor(obj);
  };
  const addColor = () => {
    let temp = [...colorsSet];
    temp.push(color);
    setcolorsSet(temp);
    console.log(colorsSet);
  };

  useEffect(() => {
    getCategoriesList();
    getBrandList();
  }, [getBrandList, getCategoriesList]);

  useEffect(() => {
    if (prod.success) {
      setSuccess("Product Added Successfully!");
      setTimeout(() => {
        setSuccess("");
        setName("");
        setModel("");
        setDescription("");
        setColor({ color: "#457899", alpha: 100 });
        setcolorsSet([]);
        setFile([]);
        setProductSuccess(false);
      }, 2000);
    }
  }, [prod.success, setProductSuccess]);
  useEffect(() => {
    const tempCat = [];
    prod.categoryList.map((i, index) => {
      let temp = {
        _id: i._id,
        name: i.name
      };
      tempCat.push(temp);
      if (index === 0) {
        setCategory(i._id);
        setSubCategory(i.subcategory[0]._id);
        setSubCategories(i.subcategory);
      }
    });
    setBrands(prod.brandList);
    prod.brandList.map((i, index) => {
      if (index === 0) {
        setBrand(i._id);
      }
    });
    setCategories(tempCat);
  }, [prod.categoryList, prod.brandList]);

  const changeName = e => {
    setName(e.target.value);
  };

  const modelNameChange = e => {
    setModel(e.target.value);
  };
  const descriptionChange = e => {
    setDescription(e.target.value);
  };

  const renderBrandOption =
    brands.length > 0 ? (
      brands.map((value, i) => {
        return <option value={value._id}>{value.name}</option>;
      })
    ) : (
      <option>No Brands</option>
    );
  const renderCategoryOption =
    categories.length > 0 ? (
      categories.map((value, i) => {
        return <option value={value._id}>{value.name}</option>;
      })
    ) : (
      <option>No Category</option>
    );

  const renderSubCategoryOption =
    subCategories.length > 0 ? (
      subCategories.map((value, i) => {
        console.log(value);
        return <option value={value._id}>{value.name}</option>;
      })
    ) : (
      <option>No SubCategory</option>
    );
  useEffect(() => {
    // action on update of movies
  }, [subCategories]);

  const setSubCategoriesData = id => {
    var data = prod.categoryList.filter(function(item) {
      return item._id === id;
    });
    console.log(data[0].subcategory);
    setSubCategories(data[0].subcategory);
  };

  const submitProduct = () => {
    setError("");
    if (category === "" || subCategory === "") {
      setError(
        "Select Category or subcategory! Please Add your Category and Subcategory"
      );
    } else if (brand === "") {
      setError("Select brand! Please Add your product brand");
    } else {
      const productData = {
        name: name,
        model: model,
        categoryId: category,
        brandId: brand,
        subCategoryId: subCategory,
        colors: colorsSet,
        description: description
      };
      addProduct(images, productData);
    }
  };
  return (
    <Col md="12">
      <Card>
        <CardHeader>
          <Row>
            <Col md="3">
              <CardTitle tag="h4">Add New Product</CardTitle>
            </Col>
            <Col mod="4" className="text-right" style={{ marginRight: 10 }}>
              <Button
                className="btn-round"
                color="danger"
                outline
                onClick={() => cancelToggle()}
              >
                Cancel
              </Button>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Form>
            <FormGroup>
              <Label for="productName">Product Name</Label>
              <Input
                type="text"
                name="productName"
                id="productName"
                placeholder="Face Primer"
                value={name}
                onChange={changeName}
              />
            </FormGroup>
            <FormGroup>
              <Label for="model">Model</Label>
              <Input
                type="text"
                name="model"
                id="model"
                placeholder="Anastasia Beverly Hills"
                value={model}
                onChange={modelNameChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="color">Selected Colors</Label>
              <Col lg="10">
                <Row>
                  {colorsSet.map((item, i) => {
                    return (
                      <div
                        style={{
                          height: "50px",
                          width: "50px",
                          backgroundColor: item.color,
                          margin: "5px",
                          opacity: item.alpha / 100
                        }}
                      ></div>
                    );
                  })}
                </Row>
              </Col>
            </FormGroup>
            <FormGroup>
              <ColorPickerPanel
                color={color.color}
                onChange={changeColor}
                mode="HSB"
              />
            </FormGroup>
            <FormGroup>
              <Button
                color="primary"
                onClick={() => {
                  addColor();
                }}
              >
                Add Color
              </Button>

              <Button
                color="danger"
                onClick={() => {
                  setcolorsSet([]);
                }}
              >
                Clear Color
              </Button>
            </FormGroup>
            <Label for="images">Product Images</Label>
            {
              <Col lg="10">
                <Row>
                  {(file || []).map(url => (
                    <img
                      style={{ width: 150, height: 150, marginRight: 10 }}
                      alt="..."
                      src={url}
                    />
                  ))}
                </Row>
              </Col>
            }

            <div style={{ height: "10px" }} />
            <Col style={{ paddingLeft: "0", paddingTop: "10" }}>
              <Input
                style={{ marginTop: "10" }}
                type="file"
                accept="image/png, image/jpeg"
                id="brandLogo"
                onChange={uploadMultipleFiles}
                multiple
              />
              <FormText color="muted">
                Click Upload button to upload the Logo of the brand.
              </FormText>
            </Col>
            <div style={{ height: "10px" }} />

            <FormGroup>
              <Label for="brand">Select Brand</Label>
              <Input
                type="select"
                style={{ height: "40px" }}
                name="selectBrand"
                id="selectBrand"
                value={brand}
                onChange={event => {
                  setBrand(event.target.value);
                }}
              >
                {renderBrandOption}
              </Input>
            </FormGroup>
            <FormGroup>
              <Row>
                <Col md="3">
                  <Label for="category">Select Category</Label>
                  <Input
                    type="select"
                    name="selectCatgory"
                    id="selectCatgory"
                    style={{ height: "40px" }}
                    value={category}
                    onChange={event => {
                      setCategory(event.target.value);
                      setSubCategoriesData(event.target.value);
                    }}
                  >
                    {renderCategoryOption}
                  </Input>
                </Col>
                <Col md="3">
                  <Label for="subCategory">Select Sub category</Label>
                  <Input
                    type="select"
                    name="selectSubCategory"
                    style={{ height: "40px" }}
                    id="selectSubCategory"
                    value={subCategory}
                    onChange={event => {
                      setSubCategory(event.target.value);
                    }}
                  >
                    {renderSubCategoryOption}
                  </Input>
                </Col>
              </Row>
            </FormGroup>
            <FormGroup>
              <Label for="description">Product Description</Label>
              <Input
                type="textarea"
                name="text"
                id="exampleText"
                value={description}
                onChange={descriptionChange}
              />
            </FormGroup>
            {error !== "" ? <Alert color="danger">{error}</Alert> : ""}
            <div style={{ height: "10px" }} />
            {success !== "" ? <Alert color="success">{success}</Alert> : ""}
            <Button color="primary" onClick={submitProduct}>
              Submit
            </Button>
          </Form>
        </CardBody>
      </Card>
    </Col>
  );
};

const mapStateToProps = state => ({
  login: state.login,
  prod: state.product
});

export default connect(mapStateToProps, {
  addProduct,
  clearProductError,
  getBrandList,
  getCategoriesList,
  setProductSuccess
})(AddProduct);
