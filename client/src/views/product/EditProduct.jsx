import React, { useState, useEffect, useRef } from "react";
import "rc-color-picker/assets/index.css";
import { Panel as ColorPickerPanel } from "rc-color-picker";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Row
} from "reactstrap";
import { connect } from "react-redux";
import {
  updateProduct,
  clearProductError,
  deactivateProduct,
  activateProduct,
  getBrandList,
  getCategoriesList
} from "../../actions/product/productActions";

const EditProduct = ({
  updateProduct,
  activateProduct,
  deactivateProduct,
  clearProductError,
  getBrandList,
  getCategoriesList,
  cancelToggle,
  prod
}) => {
  const [color, setColor] = useState({ color: "#457899", alpha: 100 });
  const [colorsSet, setcolorsSet] = useState([]);
  const [file, setFile] = useState([]);
  const [name, setName] = useState("");
  const [model, setModel] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [brands, setBrands] = useState([]);
  const [brand, setBrand] = useState("");
  const [subCategory, setSubCategory] = useState("");
  const [images, setImages] = useState("");

  const uploadMultipleFiles = e => {
    let fileObj = [];
    let imageArray = [];
    let fileArray = [];
    fileObj.push(e.target.files);
    console.log(fileObj);
    for (let i = 0; i < fileObj[0].length; i++) {
      imageArray.push(fileObj[0][i]);
    }
    setImages(imageArray);
    fileObj.push(e.target.files);
    for (let i = 0; i < fileObj[0].length; i++) {
      fileArray.push(URL.createObjectURL(fileObj[0][i]));
    }
    setFile(fileArray);
  };

  const changeColor = obj => {
    setColor(obj);
  };
  const addColor = () => {
    let temp = [...colorsSet];
    temp.push(color);
    setcolorsSet(temp);
    console.log(colorsSet);
  };

  useEffect(() => {
    getCategoriesList();
    getBrandList();
  }, [getBrandList, getCategoriesList]);

  useEffect(() => {
    setName(
      prod.product.name !== undefined && prod.product.name
        ? prod.product.name
        : ""
    );
    setDescription(
      prod.product.description !== undefined && prod.product.description
        ? prod.product.description
        : ""
    );
    setModel(
      prod.product.model !== undefined && prod.product.model
        ? prod.product.model
        : ""
    );
    setcolorsSet(
      prod.product.colors !== undefined && prod.product.colors
        ? prod.product.colors
        : []
    );
    const colorData = {
      color:
        prod.product.colors !== undefined &&
        prod.product.colors.length > 0 &&
        prod.product.colors
          ? prod.product.colors[0].color
          : "#457899",
      alpha:
        prod.product.colors !== undefined &&
        prod.product.colors.length > 0 &&
        prod.product.colors
          ? parseInt(prod.product.colors[0].aplha)
          : 100
    };
    setFile(
      prod.product.imageUrl !== undefined && prod.product.imageUrl
        ? prod.product.imageUrl
        : []
    );
    setColor(colorData);
  }, [prod.product, setCategory, setCategories]);

  useEffect(() => {
    const tempCat = [];
    prod.categoryList.map((i, index) => {
      let temp = {
        _id: i._id,
        name: i.name
      };
      tempCat.push(temp);
    });
    setBrands(prod.brandList);
    setCategories(tempCat);
    var data = prod.categoryList.filter(function(item) {
      return item._id === prod.product.categoryId;
      // console.log(item);
    });
    setCategory(data.length > 0 ? data[0]._id : "");
    setSubCategories(data.length > 0 ? data[0].subcategory : []);
    var subCat =
      data.length > 0
        ? data[0].subcategory.filter(function(item) {
            return item._id === prod.product.subCategoryId;
          })
        : [];
    setSubCategory(subCat.length > 0 ? subCat[0]._id : "");

    var brandData = prod.brandList.filter(function(item) {
      return item._id === prod.product.brandId;
      // console.log(item);
    });
    setBrand(brandData.length > 0 ? brandData[0]._id : "");
  }, [
    prod.categoryList,
    prod.brandList,
    setCategory,
    setCategories,
    setBrands,
    prod.product.categoryId,
    prod.product.subCategoryId,
    prod.product.brandId
  ]);

  const changeName = e => {
    setName(e.target.value);
  };

  const modelNameChange = e => {
    setModel(e.target.value);
  };
  const descriptionChange = e => {
    setDescription(e.target.value);
  };

  const renderBrandOption = brands.map((value, i) => {
    return <option value={value._id}>{value.name}</option>;
  });
  const renderCategoryOption = categories.map((value, i) => {
    return <option value={value._id}>{value.name}</option>;
  });

  const renderSubCategoryOption =
    subCategories.length > 0 ? (
      subCategories.map((value, i) => {
        console.log(value);
        return <option value={value._id}>{value.name}</option>;
      })
    ) : (
      <option>No SubCategory</option>
    );
  useEffect(() => {
    // action on update of movies
  }, [subCategories]);

  const setSubCategoriesData = id => {
    var data = prod.categoryList.filter(function(item) {
      return item._id === id;
    });
    console.log(data[0].subcategory);
    setSubCategories(data[0].subcategory);
  };

  const submitUpdateProduct = () => {
    const productData = {
      name: name,
      model: model,
      categoryId: category,
      brandId: brand,
      subCategoryId: subCategory,
      colors: colorsSet,
      description: description,
      imageUrl: images.length > 0 ? file : []
    };
    updateProduct(images, productData, prod.product._id);
  };
  return (
    <Col md="12">
      <Card>
        <CardHeader>
          <Row>
            <Col md="3">
              <CardTitle tag="h4">Edit Product</CardTitle>
            </Col>
            <Col mod="4" className="text-right" style={{ marginRight: 10 }}>
              <Button
                className="btn-round"
                color="danger"
                outline
                onClick={() => cancelToggle()}
              >
                Cancel
              </Button>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Form>
            <FormGroup>
              <Label for="productName">Product Name</Label>
              <Input
                type="text"
                name="productName"
                id="productName"
                placeholder="Face Primer"
                value={name}
                onChange={changeName}
              />
            </FormGroup>
            <FormGroup>
              <Label for="model">Model</Label>
              <Input
                type="text"
                name="model"
                id="model"
                placeholder="Anastasia Beverly Hills"
                value={model}
                onChange={modelNameChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="color">Selected Colors</Label>
              <Col lg="10">
                <Row>
                  {colorsSet.map((item, i) => {
                    return (
                      <div
                        style={{
                          height: "50px",
                          width: "50px",
                          backgroundColor: item.color,
                          margin: "5px",
                          opacity: item.alpha / 100
                        }}
                      ></div>
                    );
                  })}
                </Row>
              </Col>
            </FormGroup>
            <FormGroup>
              <ColorPickerPanel
                color={color.color}
                onChange={changeColor}
                mode="HSB"
              />
            </FormGroup>
            <FormGroup>
              <Button
                color="primary"
                onClick={() => {
                  addColor();
                }}
              >
                Add Color
              </Button>

              <Button
                color="danger"
                onClick={() => {
                  setcolorsSet([]);
                }}
              >
                Clear Color
              </Button>
            </FormGroup>
            <Label for="images">Product Images</Label>
            {
              <Col lg="10">
                <Row>
                  {(file || []).map(url => (
                    <img
                      style={{ width: 150, height: 150, marginRight: 10 }}
                      alt="..."
                      src={url}
                    />
                  ))}
                </Row>
              </Col>
            }

            <div style={{ height: "10px" }} />
            <Col style={{ paddingLeft: "0", paddingTop: "10" }}>
              <Input
                style={{ marginTop: "10" }}
                type="file"
                accept="image/png, image/jpeg"
                id="brandLogo"
                onChange={uploadMultipleFiles}
                multiple
              />
              <FormText color="muted">
                Click Upload button to upload the Logo of the brand.
              </FormText>
            </Col>
            <div style={{ height: "10px" }} />

            <FormGroup>
              <Label for="brand">Select Brand</Label>
              <Input
                type="select"
                style={{ height: "40px" }}
                name="selectBrand"
                id="selectBrand"
                value={brand}
                onChange={event => {
                  setBrand(event.target.value);
                }}
              >
                {renderBrandOption}
              </Input>
            </FormGroup>
            <FormGroup>
              <Row>
                <Col md="3">
                  <Label for="category">Select Category</Label>
                  <Input
                    type="select"
                    name="selectCatgory"
                    id="selectCatgory"
                    style={{ height: "40px" }}
                    value={category}
                    onChange={event => {
                      setCategory(event.target.value);
                      setSubCategoriesData(event.target.value);
                    }}
                  >
                    {renderCategoryOption}
                  </Input>
                </Col>
                <Col md="3">
                  <Label for="subCategory">Select Sub category</Label>
                  <Input
                    type="select"
                    name="selectSubCategory"
                    style={{ height: "40px" }}
                    id="selectSubCategory"
                    value={subCategory}
                    onChange={event => {
                      setSubCategory(event.target.value);
                    }}
                  >
                    {renderSubCategoryOption}
                  </Input>
                </Col>
              </Row>
            </FormGroup>
            <FormGroup>
              <Label for="description">Product Description</Label>
              <Input
                type="textarea"
                name="text"
                id="exampleText"
                value={description}
                onChange={descriptionChange}
              />
            </FormGroup>
            <Button color="primary" onClick={submitUpdateProduct}>
              Update
            </Button>
            <Button color="success" onClick={() => {}}>
              Set Default
            </Button>
            {prod.product.status ? (
              <Button
                color="danger"
                onClick={() => {
                  deactivateProduct(prod.product._id);
                }}
              >
                Set Inactive
              </Button>
            ) : (
              <Button
                color="success"
                onClick={() => {
                  activateProduct(prod.product._id);
                }}
              >
                Set Active
              </Button>
            )}
          </Form>
        </CardBody>
      </Card>
    </Col>
  );
};

const mapStateToProps = state => ({
  login: state.login,
  prod: state.product
});

export default connect(mapStateToProps, {
  updateProduct,
  clearProductError,
  activateProduct,
  deactivateProduct,
  getBrandList,
  getCategoriesList
})(EditProduct);
