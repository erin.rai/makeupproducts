import React from "react";
import { connect } from "react-redux";
import { MDBDataTable } from "mdbreact";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col,
  Button
} from "reactstrap";
import AddProduct from "./AddProduct";
import {
  setProductSuccess,
  getProducts,
  getProduct,
  getTotalProduct,
  getActiveProducts,
  getInActiveProducts
} from "../../actions/product/productActions";
import EditProduct from "./EditProduct";

class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAdd: false,
      isEdit: false,
      total: 0,
      active: 0,
      inactive: 0,
      data: {
        columns: [
          {
            label: "Name",
            field: "name",
            sort: "asc"
          },
          {
            label: "Brand Name",
            field: "brand"
          },
          {
            label: "Category",
            field: "category"
          },
          {
            label: "Status",
            field: "status"
          },
          {
            label: "Actions",
            field: "action"
          }
        ],
        rows: []
      }
    };
    this.toggle = this.toggle.bind(this);
    this.toggleEdit = this.toggleEdit.bind(this);
  }

  toggle() {
    this.setState({
      isAdd: !this.state.isAdd
    });
  }

  toggleEdit() {
    this.setState({
      isEdit: !this.state.isEdit
    });
  }

  componentWillMount() {
    this.props.getProducts();
    this.props.getTotalProduct();
    this.props.getInActiveProducts();
    this.props.getActiveProducts();
  }

  componentDidMount() {
    this.props.setProductSuccess(false);
  }

  componentDidUpdate(prevProps, prevState) {
    window.onpopstate = e => {
      if (this.state.isAdd || this.state.isEdit) {
        e.preventDefault();
        this.props.history.go(1);
        this.setState({
          isAdd: false,
          isEdit: false
        });
      }
    };
    if (
      prevProps.prod.total !== this.props.prod.total ||
      prevProps.prod.active !== this.props.prod.active ||
      prevProps.prod.inactive !== this.props.prod.inactive
    ) {
      this.setState({
        total: this.props.prod.total,
        active: this.props.prod.active,
        inactive: this.props.prod.inactive
      });
    }
    if (prevProps.prod.products !== this.props.prod.products) {
      let newRows = [...this.props.prod.products];
      let data = [];
      newRows.map(i => {
        let temp = {
          name: i.name,
          brand: i.brandId ? i.brandId.name : "No brand",
          category: i.categoryId ? i.categoryId.name : "No Category",
          status: i.status ? "Active" : "Inactive",
          action: (
            <div className="text-center">
              <Button
                onClick={() => {
                  this.props.getProduct(i._id);
                  this.toggleEdit();
                }}
                className="btn-round"
                color="primary"
                outline
                size="sm"
              >
                View
              </Button>
            </div>
          )
        };
        data.push(temp);
      });
      this.setState({
        data: {
          ...this.state.data,
          rows: data
        }
      });
      this.props.setProductSuccess(false);
    }
  }

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col lg="4" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-globe text-warning" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Products</p>
                        <CardTitle tag="p">{this.state.total}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getTotalProduct}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="4" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-money-coins text-success" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Active</p>
                        <CardTitle tag="p">{this.state.active}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getTotalProduct}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="4" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-vector text-danger" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Inactive</p>
                        <CardTitle tag="p">{this.state.inactive}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats" onClick={this.props.getTotalProduct}>
                    <i className="fas fa-sync-alt" /> Reload
                  </div>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            {this.state.isAdd ? (
              <Col>
                <AddProduct cancelToggle={() => this.toggle()} />
              </Col>
            ) : this.state.isEdit ? (
              <Col>
                <EditProduct
                  cancelToggle={() => this.toggleEdit()}
                ></EditProduct>
              </Col>
            ) : (
              <Col md="12">
                <Card>
                  <CardHeader>
                    <Row>
                      <Col md="3">
                        <CardTitle tag="h4">Product Details</CardTitle>
                      </Col>
                      <Col
                        mod="4"
                        className="text-right"
                        style={{ marginRight: 10 }}
                      >
                        <Button
                          className="btn-round"
                          color="primary"
                          type="submit"
                          outline
                          onClick={this.toggle}
                        >
                          Add Product
                        </Button>
                      </Col>
                    </Row>
                  </CardHeader>
                  <CardBody>
                    <MDBDataTable
                      bordered
                      responsiveSm
                      data={this.state.data}
                    />
                  </CardBody>
                </Card>
              </Col>
            )}
          </Row>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  login: state.login,
  prod: state.product
});

export default connect(mapStateToProps, {
  setProductSuccess,
  getProducts,
  getProduct,
  getTotalProduct,
  getActiveProducts,
  getInActiveProducts
})(Products);
