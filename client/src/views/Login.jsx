import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { loginUser } from "../actions/login/loginActions";
import {
  Col,
  Form,
  FormGroup,
  Row,
  CardBody,
  Card,
  CardTitle,
  CardHeader,
  Input,
  FormFeedback,
  Button
} from "reactstrap";

const Login = ({ login, loginUser, history }) => {
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);

  const handleUserChange = event => {
    setUser(event.target.value);
  };

  const handlePasswordChange = event => {
    setPassword(event.target.value);
  };

  onsubmit = e => {
    e.preventDefault();
    const userData = {
      user: user,
      password: password
    };
    loginUser(userData);
  };
  useEffect(() => {
    if (login.isAuthenticated) {
      history.push("/admin/dashboard");
    }
    setError(login.error);
  }, [login.isAuthenticated, login.error, history]);
  return (
    <div
      style={{
        alignItems: "center",
        justifyContent: "center",
        display: "flex",
        height: "100vh",
        //   paddingTop: "50px",
        backgroundColor: "#f4f3ef"
      }}
    >
      <Col md="5">
        <div
          style={{
            width: "100%",
            alignItems: "center",
            justifyContent: "center",
            display: "flex",
            marginBottom: "20px"
          }}
        >
          <div
            style={{
              height: "100px",
              width: "100px",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <img alt="..." src={require("assets/img/logo.png")} />
          </div>
        </div>
        <Card className="card-user">
          <CardHeader>
            <CardTitle tag="h5">Login</CardTitle>
          </CardHeader>
          <CardBody>
            <Form>
              <Row>
                <Col md="12">
                  <FormGroup>
                    <label>Email/Username</label>
                    <Input
                      placeholder="admin@gmail.com"
                      type="text"
                      value={user}
                      onChange={handleUserChange}
                      invalid={error && error.user}
                    />
                    {error && error.user ? (
                      <FormFeedback invalid>{error.user}</FormFeedback>
                    ) : null}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="12">
                  <FormGroup>
                    <label>Password</label>
                    <Input
                      placeholder="password"
                      type="password"
                      value={password}
                      invalid={error && error.password}
                      error={error && error.password ? error.password : ""}
                      onChange={handlePasswordChange}
                    />
                    {error && error.password ? (
                      <FormFeedback invalid>{error.password}</FormFeedback>
                    ) : null}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <div className="update ml-auto mr-auto">
                  {error && error.message ? (
                    <p className="text-danger">{error.message}</p>
                  ) : null}
                </div>
              </Row>
              <Row>
                <div className="update ml-auto mr-auto">
                  <Button
                    className="btn-round"
                    color="primary"
                    type="submit"
                    style={{ width: "200px" }}
                  >
                    Login
                  </Button>
                </div>
              </Row>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </div>
  );
};
const mapStateToProps = state => ({
  login: state.login
});
export default connect(mapStateToProps, { loginUser })(Login);
