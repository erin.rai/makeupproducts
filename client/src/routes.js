import Dashboard from "views/Dashboard.jsx";
import Notifications from "views/Notifications.jsx";
import Icons from "views/Icons.jsx";
import Typography from "views/Typography.jsx";
import TableList from "views/Tables.jsx";
import Maps from "views/Map.jsx";
import UserPage from "views/User.jsx";
import UpgradeToPro from "views/Upgrade.jsx";
import Brands from "views/brand/Brands.jsx";
import Products from "views/product/Products.jsx";
import Category from "views/category/Category.jsx";
import Vendor from "views/vendor/Vendor.jsx";
import Purchase from "views/purchase/Purchase.jsx";
import Stocks from "views/stock/Stocks";
var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/brands",
    name: "Brands",
    icon: "nc-icon nc-tag-content",
    component: Brands,
    layout: "/admin"
  },
  {
    path: "/products",
    name: "Products",
    icon: "nc-icon nc-tag-content",
    component: Products,
    layout: "/admin"
  },
  {
    path: "/stocks",
    name: "Stocks",
    icon: "nc-icon nc-tag-content",
    component: Stocks,
    layout: "/admin"
  },
  {
    path: "/category",
    name: "Category",
    icon: "nc-icon nc-tag-content",
    component: Category,
    layout: "/admin"
  },
  {
    path: "/vendor",
    name: "Vendor",
    icon: "nc-icon nc-tag-content",
    component: Vendor,
    layout: "/admin"
  },
  {
    path: "/purchase",
    name: "Purchase",
    icon: "nc-icon nc-tag-content",
    component: Purchase,
    layout: "/admin"
  },
  {
    path: "/icons",
    name: "Icons",
    icon: "nc-icon nc-diamond",
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/maps",
    name: "Maps",
    icon: "nc-icon nc-pin-3",
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/notifications",
    name: "Notifications",
    icon: "nc-icon nc-bell-55",
    component: Notifications,
    layout: "/admin"
  },
  {
    path: "/user-page",
    name: "User Profile",
    icon: "nc-icon nc-single-02",
    component: UserPage,
    layout: "/admin"
  }
  // {
  //   path: "/tables",
  //   name: "Table List",
  //   icon: "nc-icon nc-tile-56",
  //   component: TableList,
  //   layout: "/admin"
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   icon: "nc-icon nc-caps-small",
  //   component: Typography,
  //   layout: "/admin"
  // },
  // {
  //   pro: true,
  //   path: "/upgrade",
  //   name: "Upgrade to PRO",
  //   icon: "nc-icon nc-spaceship",
  //   component: UpgradeToPro,
  //   layout: "/admin"
  // }
];
export default routes;
