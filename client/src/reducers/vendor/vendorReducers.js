import {
  GET_VENDOR,
  GET_VENDORS,
  CLEAR_VENDOR_ERROR,
  SET_VENDOR_ERROR,
  VENDOR_SUCCESS,
  VENDOR_LOADING,
  GET_ACTIVE_VENDOR,
  GET_INACTIVE_VENDOR,
  GET_TOTAL_VENDOR
} from "../../actions/types";

const initialState = {
  loading: false,
  vendors: [],
  vendor: [],
  error: {},
  success: false,
  total: 0,
  active: 0,
  inactive: 0
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CLEAR_VENDOR_ERROR:
      return {
        ...state,
        error: {}
      };
    case SET_VENDOR_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case VENDOR_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case VENDOR_SUCCESS:
      return {
        ...state,
        success: action.payload
      };

    case GET_VENDOR:
      return {
        ...state,
        vendor: action.payload
      };
    case GET_VENDORS:
      return {
        ...state,
        vendors: action.payload
      };
    case GET_ACTIVE_VENDOR:
      return {
        ...state,
        active: action.payload
      };
    case GET_INACTIVE_VENDOR:
      return {
        ...state,
        inactive: action.payload
      };
    case GET_TOTAL_VENDOR:
      return {
        ...state,
        total: action.payload
      };
    default:
      return state;
  }
}
