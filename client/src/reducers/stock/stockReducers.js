import { GET_STOCKS, STOCK_LOADING } from "../../actions/types";

const initialState = {
  loading: false,
  stocks: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_STOCKS:
      return {
        ...state,
        stocks: action.payload
      };

    case STOCK_LOADING:
      return {
        ...state,
        loading: action.payload
      };

    default:
      return state;
  }
}
