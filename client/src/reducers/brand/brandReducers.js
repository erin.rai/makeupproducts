import {
  GET_BRAND,
  GET_BRANDS,
  CLEAR_BRAND_ERROR,
  UPDATE_BRAND,
  SET_BRAND_ERROR,
  BRAND_SUCCESS,
  BRAND_LOADING,
  GET_ACTIVE_BRAND,
  GET_INACTIVE_BRAND,
  GET_TOTAL_BRAND
} from "../../actions/types";

const initialState = {
  loading: false,
  brands: [],
  brand: [],
  error: {},
  success: false,
  total: 0,
  inactive: 0,
  active: 0
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CLEAR_BRAND_ERROR:
      return {
        ...state,
        error: {}
      };
    case SET_BRAND_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case BRAND_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case BRAND_SUCCESS:
      return {
        ...state,
        success: action.payload
      };

    case GET_BRAND:
      return {
        ...state,
        brand: action.payload
      };
    case GET_BRANDS:
      return {
        ...state,
        brands: action.payload
      };
    case GET_TOTAL_BRAND:
      return {
        ...state,
        total: action.payload
      };
    case GET_ACTIVE_BRAND:
      return {
        ...state,
        active: action.payload
      };
    case GET_INACTIVE_BRAND:
      return {
        ...state,
        inactive: action.payload
      };
    default:
      return state;
  }
}
