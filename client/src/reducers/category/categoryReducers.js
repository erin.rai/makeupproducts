import {
  GET_CATEGORIES,
  GET_CATEGORY,
  CLEAR_CATGEORY_ERROR,
  SET_CATEGORY_ERROR,
  CATEGORY_SUCCESS,
  CATEGORY_LOADING,
  GET_ACTIVE_CATEGORY,
  GET_TOTAL_CATEGORY,
  GET_INACTIVE_CATEGORY
} from "../../actions/types";

const initialState = {
  loading: false,
  categories: [],
  category: [],
  error: {},
  success: false,
  total: 0,
  inactive: 0,
  active: 0
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CLEAR_CATGEORY_ERROR:
      return {
        ...state,
        error: {}
      };
    case SET_CATEGORY_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case CATEGORY_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case CATEGORY_SUCCESS:
      return {
        ...state,
        success: action.payload
      };
    case GET_CATEGORY:
      return {
        ...state,
        category: action.payload
      };
    case GET_CATEGORIES:
      return {
        ...state,
        categories: action.payload
      };
    case GET_ACTIVE_CATEGORY:
      return {
        ...state,
        active: action.payload
      };
    case GET_INACTIVE_CATEGORY:
      return {
        ...state,
        inactive: action.payload
      };
    case GET_TOTAL_CATEGORY:
      return {
        ...state,
        total: action.payload
      };
    default:
      return state;
  }
}
