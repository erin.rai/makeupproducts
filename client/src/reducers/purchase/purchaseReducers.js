import {
  GET_PURCHASES,
  GET_PURCHASE,
  CLEAR_PURCHASE_ERROR,
  SET_PURCHASE_ERROR,
  PURCHASE_SUCCESS,
  PURCHASE_LOADING,
  GET_PRODUCT_LIST,
  GET_VENDOR_LIST,
  GET_TODAY_PURCHASE,
  GET_WEEKLY_PURCHASE,
  GET_MONTHLY_PURCHASE,
  GET_TOTAL_UNPAID
} from "../../actions/types";

const initialState = {
  loading: false,
  vendors: [],
  products: [],
  purchases: [],
  purchase: null,
  error: {},
  success: false,
  daily: 0,
  weekly: 0,
  monthly: 0,
  unpaidCount: 0
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_TODAY_PURCHASE:
      return {
        ...state,
        daily: action.payload
      };
    case GET_WEEKLY_PURCHASE:
      return {
        ...state,
        weekly: action.payload
      };
    case GET_MONTHLY_PURCHASE:
      return {
        ...state,
        monthly: action.payload
      };
    case GET_TOTAL_UNPAID:
      return {
        ...state,
        unpaidCount: action.payload
      };
    case CLEAR_PURCHASE_ERROR:
      return {
        ...state,
        error: {}
      };
    case SET_PURCHASE_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case PURCHASE_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case PURCHASE_SUCCESS:
      return {
        ...state,
        success: action.payload
      };
    case GET_PURCHASE:
      return {
        ...state,
        purchase: action.payload
      };
    case GET_PURCHASES:
      return {
        ...state,
        purchases: action.payload
      };
    case GET_PRODUCT_LIST:
      return {
        ...state,
        products: action.payload
      };
    case GET_VENDOR_LIST:
      return {
        ...state,
        vendors: action.payload
      };
    default:
      return state;
  }
}
