import {
  GET_PRODUCTS,
  GET_PORDUCT,
  CLEAR_PRODUCT_ERROR,
  SET_PRODUCT_ERROR,
  PRODUCT_SUCCESS,
  PRODUCT_LOADING,
  GET_BRAND_LIST,
  GET_CATEGORY_LIST,
  GET_ACTIVE_PRODUCT,
  GET_INACTIVE_PRODUCT,
  GET_TOTAL_PRODUCT
} from "../../actions/types";

const initialState = {
  loading: false,
  products: [],
  product: [],
  brandList: [],
  categoryList: [],
  error: {},
  success: false,
  total: 0,
  active: 0,
  inactive: 0
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CLEAR_PRODUCT_ERROR:
      return {
        ...state,
        error: {}
      };
    case GET_BRAND_LIST:
      return {
        ...state,
        brandList: action.payload
      };
    case GET_CATEGORY_LIST:
      return {
        ...state,
        categoryList: action.payload
      };
    case SET_PRODUCT_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case PRODUCT_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case PRODUCT_SUCCESS:
      return {
        ...state,
        success: action.payload
      };
    case GET_PORDUCT:
      return {
        ...state,
        product: action.payload
      };
    case GET_PRODUCTS:
      return {
        ...state,
        products: action.payload
      };
    case GET_ACTIVE_PRODUCT:
      return {
        ...state,
        active: action.payload
      };
    case GET_INACTIVE_PRODUCT:
      return {
        ...state,
        inactive: action.payload
      };
    case GET_TOTAL_PRODUCT:
      return {
        ...state,
        total: action.payload
      };
    default:
      return state;
  }
}
