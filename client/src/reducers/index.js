import { combineReducers } from "redux";
import loginReducer from "./login/loginReducer";
import brandReducer from "./brand/brandReducers";
import categoryReducer from "./category/categoryReducers";
import productReducer from "./product/productReducers";
import vendorReducer from "./vendor/vendorReducers";
import purchaseReducer from "./purchase/purchaseReducers";
import stockReducer from "./stock/stockReducers";

export default combineReducers({
  login: loginReducer,
  brand: brandReducer,
  category: categoryReducer,
  product: productReducer,
  vendor: vendorReducer,
  purchase: purchaseReducer,
  stock: stockReducer
});
