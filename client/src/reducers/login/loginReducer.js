import {
  SET_CURRENT_USER,
  CLEAR_LOGIN_ERROR,
  LOGIN_ERRORS
} from "../../actions/types";

const isEmpty = require("is-empty");

const initialState = {
  isAuthenticated: false,
  user: {},
  loading: false,
  error: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        error: {},
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };
    case CLEAR_LOGIN_ERROR:
      return {
        ...state,
        error: {}
      };
    case LOGIN_ERRORS:
      return {
        ...state,
        error: action.payload
      };
    default:
      return state;
  }
}
