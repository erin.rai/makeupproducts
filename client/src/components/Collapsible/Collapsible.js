import React, { Component } from "react";
import "./Collapsible.css";
import { Button, Col } from "reactstrap";

export default class Collapsible extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
    this.togglePanel = this.togglePanel.bind(this);
  }
  togglePanel(e) {
    this.setState({ open: !this.state.open });
  }
  render() {
    return (
      <div>
        <div onClick={e => this.togglePanel(e)} className="header">
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <div>{this.props.title}</div>
            <div>
              {!this.state.open ? (
                <Col md="4" xs="5">
                  <div className="icon-big text-center ">
                    <i className="nc-icon nc-minimal-down" />
                  </div>
                </Col>
              ) : (
                <Col md="4" xs="5">
                  <div className="icon-big text-center ">
                    <i className="nc-icon nc-minimal-up" />
                  </div>
                </Col>
              )}
            </div>
          </div>
        </div>
        {this.state.open ? (
          <div className="content">
            <ul>
              <li style={{ listStyleType: "decimal" }}>asd</li>
              <li style={{ listStyleType: "decimal" }}>asd</li>
            </ul>
            <Col className="text-right" style={{ paddingBottom: 10 }}>
              <Button
                className="btn-round"
                color="primary"
                outline
                size="sm"
                onClick={this.props.toggleSubCat}
              >
                View
              </Button>
            </Col>
          </div>
        ) : null}
      </div>
    );
  }
}
