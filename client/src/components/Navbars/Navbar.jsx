import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
  Input,
  Row,
  Col,
  Button
} from "reactstrap";

import routes from "routes.js";

import { logoutUser } from "../../actions/login/loginActions";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      dropdownOpen: false,
      color: "transparent",
      settingOpen: false,
      name: "/"
    };
    this.toggle = this.toggle.bind(this);
    this.settingToggle = this.settingToggle.bind(this);
    this.dropdownToggle = this.dropdownToggle.bind(this);
    this.sidebarToggle = React.createRef();
  }

  toggle() {
    if (this.state.isOpen) {
      this.setState({
        color: "transparent"
      });
    } else {
      this.setState({
        color: "dark"
      });
    }
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  dropdownToggle(e) {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  settingToggle(e) {
    this.setState({
      settingOpen: !this.state.settingOpen
    });
  }
  getBrand() {
    let brandName = "Default Brand";
    routes.map((prop, key) => {
      if (window.location.href.indexOf(prop.layout + prop.path) !== -1) {
        brandName = prop.name;
      }
      return null;
    });
    return brandName;
  }

  getName() {
    let name = "/";
    routes.map((prop, key) => {
      if (window.location.href.indexOf(prop.layout + prop.path) !== -1) {
        name = prop.layout + prop.path;
      }
      return null;
    });
    return name;
  }

  openSidebar() {
    document.documentElement.classList.toggle("nav-open");
    this.sidebarToggle.current.classList.toggle("toggled");
  }
  // function that adds color dark/transparent to the navbar on resize (this is for the collapse)
  updateColor() {
    if (window.innerWidth < 993 && this.state.isOpen) {
      this.setState({
        color: "dark"
      });
    } else {
      this.setState({
        color: "transparent"
      });
    }
  }
  componentDidMount() {
    window.addEventListener("resize", this.updateColor.bind(this));
  }
  componentDidUpdate(e) {
    if (
      window.innerWidth < 993 &&
      e.history.location.pathname !== e.location.pathname &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
      this.sidebarToggle.current.classList.toggle("toggled");
    }
  }
  render() {
    return (
      // add or remove classes depending if we are on full-screen-maps page or not
      <Navbar
        color={
          this.props.location.pathname.indexOf("full-screen-maps") !== -1
            ? "dark"
            : this.state.color
        }
        expand="lg"
        className={
          this.props.location.pathname.indexOf("full-screen-maps") !== -1
            ? "navbar-absolute fixed-top"
            : "navbar-absolute fixed-top " +
              (this.state.color === "transparent" ? "navbar-transparent " : "")
        }
      >
        <Container fluid>
          <div className="navbar-wrapper">
            <div className="navbar-toggle">
              <button
                type="button"
                ref={this.sidebarToggle}
                className="navbar-toggler"
                onClick={() => this.openSidebar()}
              >
                <span className="navbar-toggler-bar bar1" />
                <span className="navbar-toggler-bar bar2" />
                <span className="navbar-toggler-bar bar3" />
              </button>
            </div>
            <NavbarBrand href={this.getName()}>{this.getBrand()}</NavbarBrand>
          </div>
          <NavbarToggler onClick={this.toggle}>
            <span className="navbar-toggler-bar navbar-kebab" />
            <span className="navbar-toggler-bar navbar-kebab" />
            <span className="navbar-toggler-bar navbar-kebab" />
          </NavbarToggler>
          <Collapse
            isOpen={this.state.isOpen}
            navbar
            className="justify-content-end"
          >
            <form>
              <InputGroup className="no-border">
                <Input placeholder="Search..." />
                <InputGroupAddon addonType="append">
                  <InputGroupText>
                    <i className="nc-icon nc-zoom-split" />
                  </InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </form>
            <Nav navbar>
              <NavItem>
                <Link to="/" className="nav-link btn-magnify">
                  <i className="nc-icon nc-layout-11" />
                  <p>
                    <span className="d-lg-none d-md-block">Stats</span>
                  </p>
                </Link>
              </NavItem>
              <Dropdown
                nav
                isOpen={this.state.dropdownOpen}
                toggle={e => this.dropdownToggle(e)}
              >
                <DropdownToggle caret nav>
                  <i className="nc-icon nc-bell-55" />
                  <p>
                    <span className="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem tag="a">
                    This is the first notification hello world
                  </DropdownItem>
                  <DropdownItem tag="a">
                    This is the second notification What the hell
                  </DropdownItem>
                  <DropdownItem tag="a">
                    This is the first notification qweresa
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
              <Dropdown
                nav
                isOpen={this.state.settingOpen}
                toggle={e => this.settingToggle(e)}
              >
                <DropdownToggle caret nav>
                  <i className="nc-icon nc-settings-gear-65" />
                  <p>
                    <span className="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem tag="a">
                    <Row>
                      <Col xs="1" style={{ paddingLeft: 15, paddingTop: 2 }}>
                        <span
                          data-notify="icon"
                          className="nc-icon nc-settings-gear-65"
                        />
                      </Col>
                      <Col md="1" style={{ paddingLeft: 5 }}>
                        <span>Settings</span>
                      </Col>
                    </Row>
                  </DropdownItem>
                  <DropdownItem href="/admin/user-page">
                    <Row>
                      <Col xs="1" style={{ paddingLeft: 15, paddingTop: 2 }}>
                        <span
                          data-notify="icon"
                          className="nc-icon nc-single-02"
                        />
                      </Col>
                      <Col md="1" style={{ paddingLeft: 5 }}>
                        <span>Profile</span>
                      </Col>
                    </Row>
                  </DropdownItem>
                  <DropdownItem tag="a">
                    <Row>
                      <Col xs="1" style={{ paddingLeft: 15, paddingTop: 2 }}>
                        <span
                          data-notify="icon"
                          className="nc-icon nc-email-85"
                        />
                      </Col>
                      <Col md="1" style={{ paddingLeft: 5 }}>
                        <span>Messages</span>
                      </Col>
                    </Row>
                  </DropdownItem>
                  <DropdownItem tag="a">
                    <Row>
                      <Col xs="1" style={{ paddingLeft: 15, paddingTop: 2 }}>
                        <span
                          data-notify="icon"
                          className="nc-icon nc-button-power"
                        />
                      </Col>
                      <Col md="1" style={{ paddingLeft: 5 }}>
                        <span onClick={this.props.logoutUser}>Logout</span>
                      </Col>
                    </Row>
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}
const mapStateToProps = state => ({
  login: state.login
});

export default connect(mapStateToProps, { logoutUser })(Header);
