const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function adminValidateLoginInput(data) {
  let errors = {};
  data.user = !isEmpty(data.user) ? data.user : "";
  data.password = !isEmpty(data.password) ? data.password : "";

  if (Validator.isEmpty(data.user)) {
    errors.user = "Username or Email is required field!";
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = "Password is required field!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
