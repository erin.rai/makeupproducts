const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function validateSubCategory(data) {
  let errors = {};
  data.name = !isEmpty(data.name) ? data.name : "";

  if (Validator.isEmpty(data.name)) {
    errors.subcategory = "Sub category name is a required field!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
