const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function validateBrand(data) {
  let errors = {};
  data.name = !isEmpty(data.name) ? data.name : "";

  if (Validator.isEmpty(data.name)) {
    errors.product = "Product name is a required field!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
