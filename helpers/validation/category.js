const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function validateCategory(data) {
  let errors = {};
  data.name = !isEmpty(data.name) ? data.name : "";

  if (Validator.isEmpty(data.name)) {
    errors.category = "Category name is a required field!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
