const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function validateVendor(data) {
  let errors = {};
  data.name = !isEmpty(data.name) ? data.name : "";

  if (Validator.isEmpty(data.name)) {
    errors.name = "Vendor name is a required field!";
  }
  if (Validator.isEmpty(data.email)) {
    errors.email = "Vendor email is a required field!";
  } else if (!Validator.isEmpty(data.email)) {
    if (!Validator.isEmail(data.email)) {
      errors.email = "Email is invalid";
    }
  }
  if (Validator.isEmpty(data.phone)) {
    errors.phone = "Vendor phone is a required field!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
