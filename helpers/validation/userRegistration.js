const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function validateAdminRegistrationInput(data) {
  let errors = {};

  data.email = !isEmpty(data.email) ? data.email : "";
  data.contactno = !isEmpty(data.contactno) ? data.contactno : "";
  data.userName = !isEmpty(data.userName) ? data.userName : "";
  data.password = !isEmpty(data.password) ? data.password : "";
  data.rePassword = !isEmpty(data.rePassword) ? data.rePassword : "";

  if (Validator.isEmpty(data.email)) {
    errors.email = "Email is a required field!";
  } else if (!Validator.isEmail(data.email)) {
    errors.email = "Email is invalid!";
  }

  if (Validator.isEmpty(data.contactno)) {
    errors.contactno = "Contactno is a required field!";
  } else if (!Validator.isLength(data.contactno, { min: 10, max: 20 })) {
    errors.contactno = "Contact number is invalid!";
  }

  if (Validator.isEmpty(data.userName)) {
    errors.userName = "Usern name is required field!";
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = "Password is required field";
  } else if (!Validator.isLength(data.password, { min: 6 })) {
    errors.password = "Password must be at least 6 character!";
  } else if (Validator.isEmpty(data.rePassword)) {
    errors.rePassword = "Retype Password is required field";
  } else if (data.password !== data.rePassword) {
    errors.rePassword = "Password does not match!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
