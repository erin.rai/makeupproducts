const multer = require("multer");
const fs = require("fs");
var mkdirp = require("mkdirp");

//upload image
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    console.log(req.url);
    var dir = `public${req.url}`;
    // console.log(!fs.existsSync(dir));
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir, function(err) {
        if (err) console.error("error" + err);
        else console.log("creating error");
      });
    }
    cb(null, `./public${req.url}`);
  },
  filename: function(req, file, cb) {
    cb(
      null,
      `${new Date().toISOString().replace(/:/g, "-")}` + file.originalname
    );
  }
});

const fileFilter = (req, file, cb) => {
  //reject a file
  if (
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "application/octet-stream"
  ) {
    cb(null, true);
  } else {
    req.fileValidationError = "Forbidden extension";
    cb(null, false, req.fileValidationError);
  }
};

var imageUpload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

module.exports.imageUpload = imageUpload;
