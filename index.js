require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const passport = require("passport");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv").config();
const app = express();
const init = require("./config/init");
const mongoURI = process.env.MONGO_URI;
const PORT = process.env.PORT || 5000;
const adminAuth = require("./routes/api/adminAuth");
const userAuth = require("./routes/api/userAuth");
const brand = require("./routes/api/brand");
const category = require("./routes/api/category");
const product = require("./routes/api/product");
const vendor = require("./routes/api/vendor");
const purchase = require("./routes/api/purchase");
const stock = require("./routes/api/stock");

app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(bodyParser.json());

mongoose
  .connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("Mogodb connected successfully"))
  .catch(err => console.log(err));

app.use(passport.initialize());
app.use(passport.session());
app.use(express.static("public"));

require("./config/adminPassport")(passport);
// require("./config/userPassport")(passport);
app.use("/api/admin", adminAuth);
app.use("/api/user", userAuth);
app.use("/api/brand", brand);
app.use("/api/category", category);
app.use("/api/product", product);
app.use("/api/vendor", vendor);
app.use("/api/purchase", purchase);
app.use("/api/stock", stock);

init(app, mongoURI).then(function initialized() {
  app.listen(PORT, () => console.log(`Server up and running on port ${PORT}`));
});
