const express = require("express");
const router = express.Router();
const stockController = require("../controller/stock");
const passport = require("passport");

router.get(
  "/getStocks",
  passport.authenticate("jwt", { session: true }),
  stockController.getAllStock
);

router.get(
  "/getStock/:stockId",
  passport.authenticate("jwt", { session: true }),
  stockController.getStock
);

module.exports = router;
