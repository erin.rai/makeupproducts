const express = require("express");
const router = express.Router();
const productController = require("../controller/product");
const passport = require("passport");
const imageUpload = require("../../helpers/functions/imageupload");

router.get(
  "/getProducts",
  passport.authenticate("jwt", { session: true }),
  productController.getAllProduct
);
router.get(
  "/getProductsList",
  passport.authenticate("jwt", { session: true }),
  productController.getAllProductList
);

router.get(
  "/getProduct/:productId",
  passport.authenticate("jwt", { session: true }),
  productController.getProduct
);

router.post(
  "/addProduct",
  passport.authenticate("jwt", { session: true }),
  productController.addProduct
);

router.patch(
  "/updateProduct/:productId",
  passport.authenticate("jwt", { session: true }),
  productController.updateProduct
);

router.patch(
  "/activateProduct/:productId",
  passport.authenticate("jwt", { session: true }),
  productController.activateProduct
);

router.patch(
  "/deactivateProduct/:productId",
  passport.authenticate("jwt", { session: true }),
  productController.deActivateProduct
);

router.post(
  "/productImages",
  passport.authenticate("jwt", { session: true }),
  imageUpload.imageUpload.any("productimages"),
  productController.uploadProductImages
);

router.get(
  "/getProductCount",
  passport.authenticate("jwt", { session: true }),
  productController.countProduct
);

router.get(
  "/getActiveProductCount",
  passport.authenticate("jwt", { session: true }),
  productController.countActiveProduct
);
router.get(
  "/getInActiveProductCount",
  passport.authenticate("jwt", { session: true }),
  productController.countInActiveProduct
);

module.exports = router;
