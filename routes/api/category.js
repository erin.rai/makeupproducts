const express = require("express");
const router = express.Router();
const categoryController = require("../controller/category");
const passport = require("passport");

router.post(
  "/addCategory",
  passport.authenticate("jwt", { session: true }),
  categoryController.addCategory
);
router.post(
  "/addSubcategory/:categoryId",
  passport.authenticate("jwt", { session: true }),
  categoryController.addSubcategory
);
router.get(
  "/getCategories",
  passport.authenticate("jwt", { session: true }),
  categoryController.getAllCategory
);
router.get(
  "/getCategory/:categoryId",
  passport.authenticate("jwt", { session: true }),
  categoryController.getCategory
);
router.patch(
  "/updateSubCategory/:categoryId",
  passport.authenticate("jwt", { session: true }),
  categoryController.updateSubcategory
);
router.patch(
  "/updateCategory/:categoryId",
  passport.authenticate("jwt", { session: true }),
  categoryController.updateCategory
);

router.patch(
  "/activateCategory/:categoryId",
  passport.authenticate("jwt", { session: true }),
  categoryController.activateCategory
);

router.patch(
  "/deactivateCategory/:categoryId",
  passport.authenticate("jwt", { session: true }),
  categoryController.deactivateCategory
);

router.patch(
  "/activateSubCategory/:categoryId",
  passport.authenticate("jwt", { session: true }),
  categoryController.activateSubcategory
);
router.patch(
  "/deactivateSubCategory/:categoryId",
  passport.authenticate("jwt", { session: true }),
  categoryController.deactivateSubcategory
);

router.get(
  "/getAllCategoryList",
  passport.authenticate("jwt", { session: true }),
  categoryController.getAllCategoryList
);
router.get(
  "/getCategoryCount",
  passport.authenticate("jwt", { session: true }),
  categoryController.countCategory
);

router.get(
  "/getActiveCategoryCount",
  passport.authenticate("jwt", { session: true }),
  categoryController.countActiveCategory
);
router.get(
  "/getInActiveCategoryCount",
  passport.authenticate("jwt", { session: true }),
  categoryController.countInActiveCategory
);
module.exports = router;
