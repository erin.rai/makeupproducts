const express = require("express");
const router = express.Router();
const adminUserController = require("../controller/adminUserAuth");

router.post("/login", adminUserController.loginAdminUser);
module.exports = router;
