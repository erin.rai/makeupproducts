const express = require("express");
const router = express.Router();
const purchaseController = require("../controller/purchase");
const passport = require("passport");

router.get(
  "/getPurchases",
  passport.authenticate("jwt", { session: true }),
  purchaseController.getAllPurchase
);
router.get(
  "/getAllProductList",
  passport.authenticate("jwt", { session: true }),
  purchaseController.getAllProductList
);
router.get(
  "/getAllVendorList",
  passport.authenticate("jwt", { session: true }),
  purchaseController.getAllVendorList
);
router.get(
  "/getPurchase/:purchaseId",
  passport.authenticate("jwt", { session: true }),
  purchaseController.getPurchase
);

router.post(
  "/addPurchase",
  passport.authenticate("jwt", { session: true }),
  purchaseController.addPurchase
);

router.patch(
  "/updatePurchase/:purchaseId",
  passport.authenticate("jwt", { session: true }),
  purchaseController.updatePurchase
);

router.get(
  "/todayPurchase",
  passport.authenticate("jwt", { session: true }),
  purchaseController.countTodayPurchase
);

router.get(
  "/weeklyPurchase",
  passport.authenticate("jwt", { session: true }),
  purchaseController.countLastWeekDayPurchase
);
router.get(
  "/monthlyPurchase",
  passport.authenticate("jwt", { session: true }),
  purchaseController.countLastThirtyDayPurchase
);
router.get(
  "/totalUnpaid",
  passport.authenticate("jwt", { session: true }),
  purchaseController.totalUnpaid
);

module.exports = router;
