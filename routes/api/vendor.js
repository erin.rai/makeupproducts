const express = require("express");
const router = express.Router();
const vendorController = require("../controller/vendor");
const passport = require("passport");

router.post(
  "/addVendor",
  passport.authenticate("jwt", { session: true }),
  vendorController.addVendor
);
router.get(
  "/getVendors",
  passport.authenticate("jwt", { session: true }),
  vendorController.getAllVendor
);
router.get(
  "/getVendor/:vendorId",
  passport.authenticate("jwt", { session: true }),
  vendorController.getVendor
);
router.patch(
  "/updateVendor/:vendorId",
  passport.authenticate("jwt", { session: true }),
  vendorController.updateVendor
);
router.patch(
  "/updateVendor/activate/:vendorId",
  passport.authenticate("jwt", { session: true }),
  vendorController.activateVendor
);
router.patch(
  "/updateVendor/deactivate/:vendorId",
  passport.authenticate("jwt", { session: true }),
  vendorController.deactivateVendor
);

router.get(
  "/getAllVendorList",
  passport.authenticate("jwt", { session: true }),
  vendorController.getAllVendorList
);
router.get(
  "/getVendorCount",
  passport.authenticate("jwt", { session: true }),
  vendorController.countVendor
);

router.get(
  "/getActiveVendorCount",
  passport.authenticate("jwt", { session: true }),
  vendorController.countActiveVendor
);
router.get(
  "/getInActiveVendorCount",
  passport.authenticate("jwt", { session: true }),
  vendorController.countInActiveVendor
);
module.exports = router;
