const express = require("express");
const router = express.Router();
const brandController = require("../controller/brand");
const passport = require("passport");
const imageUpload = require("../../helpers/functions/imageupload");

router.post(
  "/addBrand",
  passport.authenticate("jwt", { session: true }),
  brandController.addBrand
);
router.get(
  "/getBrands",
  passport.authenticate("jwt", { session: true }),
  brandController.getAllBrand
);
router.get(
  "/getBrand/:brandId",
  passport.authenticate("jwt", { session: true }),
  brandController.getBrand
);
router.patch(
  "/updateBrand/:brandId",
  passport.authenticate("jwt", { session: true }),
  brandController.updateBrand
);
router.patch(
  "/updateBrand/activate/:brandId",
  passport.authenticate("jwt", { session: true }),
  brandController.activateBrand
);
router.patch(
  "/updateBrand/deactivate/:brandId",
  passport.authenticate("jwt", { session: true }),
  brandController.deactivateBrand
);
router.post(
  "/brandImage",
  passport.authenticate("jwt", { session: true }),
  imageUpload.imageUpload.single("brandimage"),
  brandController.uploadBrandImage
);

router.get(
  "/getAllBrandList",
  passport.authenticate("jwt", { session: true }),
  brandController.getAllBrandList
);

router.get(
  "/getBrandCount",
  passport.authenticate("jwt", { session: true }),
  brandController.countBrand
);

router.get(
  "/getActiveBrandCount",
  passport.authenticate("jwt", { session: true }),
  brandController.countActiveBrand
);
router.get(
  "/getInActiveBrandCount",
  passport.authenticate("jwt", { session: true }),
  brandController.countInActiveBrand
);

module.exports = router;
