const User = require("../../models/user");

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const key = process.env.SECRETORKEY;
const validateAdminRegistrationInput = require("../../helpers/validation/userRegistration");
const validateLoginInput = require("../../helpers/validation/loginValidation");

exports.registerUser = (req, res, next) => {
  const { errors, isValid } = validateAdminRegistrationInput(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }
  const email = req.body.email;
  const password = req.body.password;
  const contactno = req.body.contactno;
  const userName = req.body.userName;

  User.findOne({
    $or: [
      {
        email: email
      },
      {
        contactno: contactno
      }
    ]
  })
    .then(user => {
      if (user) {
        return res.status(400).json({
          success: false,
          payload: "Email or contactno already exist!"
        });
      }
      var newUser = new User({
        email: email,
        password: password,
        contactno: contactno,
        userName: userName
      });

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser.save(user => {
            res.status(201).json({
              success: true,
              payload: { message: "Registration Successful!" }
            });
          });
        });
      });
    })
    .catch(e => next(e));
};

exports.loginUser = (req, res, next) => {
  const { errors, isValid } = validateLoginInput(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }
  const user = req.body.user;
  const password = req.body.password;
  User.findOne({
    $or: [
      {
        userName: user
      },
      {
        email: user
      },
      {
        contactno: user
      }
    ]
  })
    .then(user => {
      if (!user) {
        return res
          .status(404)
          .json({ success: false, payload: "User does not exist!" });
      }
      bcrypt.compare(password, user.password).then(isMatch => {
        if (isMatch) {
          const payload = JSON.parse(JSON.stringify(user));
          jwt.sign(
            payload,
            key,
            {
              expiresIn: "1h"
            },
            (err, token) => {
              res.json({
                success: true,
                token: "Bearer " + token
              });
            }
          );
        } else {
          return res
            .status(400)
            .json({
              success: false,
              payload: { message: "Incorrect password!" }
            });
        }
      });
    })
    .catch(e => next(e));
};
