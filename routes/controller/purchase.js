const moment = require("moment");
const Stock = require("../../models/stock");
const Purchase = require("../../models/purchase");
const Product = require("../../models/product");
const Vendor = require("../../models/vendor");

function saveProducts(req, callback) {
  var count = 0;
  const products = req.body.products;
  products.map(i => {
    Stock.findOne({ productId: i._id })
      .then(stock => {
        if (stock) {
          Stock.updateOne(
            { productId: i._id },
            {
              $inc: { quantity: i.quantity },
              $set: { unitPrice: i.sellPrice, buyPrice: i.buyPrice }
            }
          )
            .then(stock => {
              if (stock) {
                count++;
                if (count == products.length) {
                  return callback(true);
                }
              }
            })
            .catch(e => console.log(e));
        } else {
          const newStock = new Stock({
            productId: i._id,
            quantity: i.quantity,
            unitPrice: i.sellPrice,
            buyPrice: i.buyPrice
          });
          newStock
            .save()
            .then(stock => {
              if (stock) {
                count++;
                if (count == products.length) {
                  return callback(true);
                }
              }
            })
            .catch(e => console.log(e));
        }
      })
      .catch(e => console.log(e));
  });
}

exports.addPurchase = (req, res) => {
  saveProducts(req, function(response) {
    if (response) {
      const newPurchase = new Purchase({
        products: req.body.products,
        total: req.body.total,
        userId: req.user._id,
        tax: req.body.tax,
        discount: req.body.discount,
        vendorId: req.body.vendorId,
        paid: req.body.paid
      });
      newPurchase
        .save()
        .then(purchase => {
          if (purchase) {
            res.json({
              success: true,
              payload: { message: "Purchase made Successfully!" }
            });
          }
        })
        .catch(e => console.log(e));
    } else {
      res.json({
        success: false,
        payload: { message: "Purchase made Failed! Please try again!" }
      });
    }
  });
};

exports.getAllPurchase = (req, res) => {
  Purchase.aggregate([
    {
      $lookup: {
        from: "vendors",
        localField: "vendorId",
        foreignField: "_id",
        as: "Ven"
      }
    },
    {
      $project: {
        products: { $size: "$products" },
        total: "$total",
        paid: "$paid",
        purchaseDate: "$purchaseDate",
        vendor: "$Ven"
      }
    }
  ])
    .sort({ created: -1 })
    .then((purchases, err) => {
      if (err) {
        res.json({
          success: false,
          payload: { message: "Error fetching data" }
        });
      } else {
        if (!purchases) {
          res.json({
            success: false,
            payload: { message: "No Purchases Found" }
          });
        }
        res.json({ success: true, payload: purchases });
      }
    })
    .catch(e => console.log(e));
};

exports.getPurchase = (req, res) => {
  Purchase.findOne({ _id: req.params.purchaseId }).then((purchase, err) => {
    if (err) {
      res.json({
        success: false,
        payload: { message: "Error Fetching purchase" }
      });
    } else {
      if (!purchase) {
        res.json({ success: false, payload: { message: "No Purchase Found" } });
      }
      res.json({ success: true, payload: purchase });
    }
  });
};

exports.getAllProductList = (req, res) => {
  Product.find({ status: true }, { name: 1 })
    .sort({ created: -1 })
    .then((prods, err) => {
      if (err) {
        res.json({
          success: false,
          payload: { message: "Error fetching products" }
        });
      } else {
        if (!prods) {
          res.json({
            success: false,
            payload: { message: "No Products Found!" }
          });
        }
      }
      res.json({ success: true, payload: prods });
    })
    .catch(e => console.log(e));
};

exports.getAllVendorList = (req, res) => {
  Vendor.find({ active: true }, { name: 1 })
    .sort({ created: -1 })
    .then((prods, err) => {
      if (err) {
        res.json({
          success: false,
          payload: { message: "Error fetching products" }
        });
      } else {
        if (!prods) {
          res.json({
            success: false,
            payload: { message: "No Products Found!" }
          });
        }
      }
      res.json({ success: true, payload: prods });
    })
    .catch(e => console.log(e));
};

exports.updatePurchase = (req, res) => {
  Purchase.findOneAndUpdate(
    { _id: req.params.purchaseId },
    {
      $set: {
        paid: true
      }
    },
    { new: true, useFindAndModify: false }
  )
    .then(purchase => {
      res.json({ success: true, payload: { message: "Purchase paid!" } });
    })
    .catch(e => console.log(e));
};

var start = moment().startOf("day");
var end = moment().endOf("day");
// var pipeline = [
//   {
//     $match: {
//       purchaseDate: { $gte: start, $lt: end }
//     }
//   },
//   {
//     $group: {
//       _id: null,
//       count: { $sum: 1 }
//     }
//   }
// ];

exports.countTodayPurchase = (req, res) => {
  Purchase.find({
    purchaseDate: { $gte: start, $lt: end }
  })
    .countDocuments()
    .then(count => {
      res.json({ success: true, payload: { count } });
    })
    .catch(e => console.log(e));
};

exports.countLastWeekDayPurchase = (req, res) => {
  Purchase.find({
    purchaseDate: { $gte: new Date(new Date() - 7 * 60 * 60 * 24 * 1000) }
  })
    .countDocuments()
    .then(count => {
      res.json({ success: true, payload: { count } });
    })
    .catch(e => console.log(e));
};
exports.countLastThirtyDayPurchase = (req, res) => {
  Purchase.find({
    purchaseDate: { $gte: new Date(new Date() - 30 * 60 * 60 * 24 * 1000) }
  })
    .countDocuments()
    .then(count => {
      res.json({ success: true, payload: { count } });
    })
    .catch(e => console.log(e));
};

exports.totalUnpaid = (req, res) => {
  Purchase.find({
    paid: false
  })
    .countDocuments()
    .then(count => {
      res.json({ success: true, payload: { count } });
    })
    .catch(e => console.log(e));
};

// function saveProducts(req, callback) {
//   var count = 0;
//   const products = req.body.products;
//   products.map(i => {
//     Stock.findOne({ productId: i._id })
//       .then(stock => {
//         if (stock) {
//           Stock.updateOne(
//             { productId: i._id },
//             {
//               $inc: { quantity: i.quantity },
//               $set: { unitPrice: i.sellPrice }
//             }
//           )
//             .then(stock => {
//               if (stock) {
//                 count++;
//                 if (count == products.length) {
//                   return callback(true);
//                 }
//               }
//             })
//             .catch(e => console.log(e));
//         } else {
//           const newStock = new Stock({
//             productId: i._id,
//             quantity: i.quantity,
//             unitPrice: i.sellPrice
//           });
//           newStock
//             .save()
//             .then(stock => {
//               if (stock) {
//                 count++;
//                 if (count == products.length) {
//                   return callback(true);
//                 }
//               }
//             })
//             .catch(e => console.log(e));
//         }
//       })
//       .catch(e => console.log(e));
//   });
// }
