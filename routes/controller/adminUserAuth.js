const AdminUser = require("../../models/admin");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const key = process.env.SECRETORKEY;
const adminLoginValidation = require("../../helpers/validation/adminLogin");

exports.loginAdminUser = (req, res, next) => {
  const { errors, isValid } = adminLoginValidation(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }
  const user = req.body.user;
  const password = req.body.password;
  AdminUser.findOne({
    $or: [
      {
        username: user
      },
      {
        email: user
      }
    ]
  })
    .then(adminUser => {
      if (!adminUser) {
        return res.status(404).json({
          success: false,
          payload: { message: "User does not exist!" }
        });
      }
      bcrypt.compare(password, adminUser.password).then(isMatch => {
        if (isMatch) {
          const payload = JSON.parse(JSON.stringify(adminUser));
          jwt.sign(
            payload,
            key,
            {
              expiresIn: "1h"
            },
            (err, token) => {
              res.json({
                success: true,
                token: "Bearer " + token
              });
            }
          );
        } else {
          return res.status(400).json({
            success: false,
            payload: { message: "Incorrect password!" }
          });
        }
      });
    })
    .catch(e => next(e));
};
