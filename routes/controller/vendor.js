const Vendor = require("../../models/vendor");
const validateVendor = require("../../helpers/validation/vendor");

exports.countVendor = (req, res) => {
  Vendor.find().countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};
exports.countActiveVendor = (req, res) => {
  Vendor.find({ active: true }).countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};
exports.countInActiveVendor = (req, res) => {
  Vendor.find({ active: false }).countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};

exports.addVendor = (req, res) => {
  const { errors, isValid } = validateVendor(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }

  Vendor.findOne({
    name: req.params.name
  }).then(vendor => {
    if (vendor) {
      return res.status(404).json({
        success: false,
        payload: { message: "Vendor name already exists!" }
      });
    }
    const newVendor = new Vendor({
      name: req.body.name,
      email: req.body.email,
      phone: req.body.phone,
      url: req.body.url,
      address: req.body.address
    });
    newVendor
      .save()
      .then(vendor =>
        res.json({
          success: true,
          payload: { message: "New Vendor added successfully" }
        })
      )
      .catch(e => console.log(e));
  });
};

exports.updateVendor = (req, res) => {
  const { errors, isValid } = validateVendor(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }
  Vendor.findOneAndUpdate(
    { _id: req.params.vendorId },
    {
      $set: {
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        url: req.body.url,
        address: req.body.address,
        updated: Date.now()
      }
    },
    { new: true, useFindAndModify: false }
  )
    .then(vendor => {
      res.json({ success: true, payload: { message: "Vendor updated!" } });
    })
    .catch(e => console.log(e));
};

exports.activateVendor = (req, res) => {
  Vendor.findOneAndUpdate(
    { _id: req.params.vendorId },
    {
      $set: {
        active: true
      }
    },
    { new: true, useFindAndModify: false }
  )
    .then(vendor => {
      res.json({ success: true, payload: { message: "Vendor activated!" } });
    })
    .catch(e => console.log(e));
};
exports.deactivateVendor = (req, res) => {
  Vendor.findOneAndUpdate(
    { _id: req.params.vendorId },
    {
      $set: {
        active: false
      }
    },
    { new: true, useFindAndModify: false }
  )
    .then(vendor => {
      res.json({ success: true, payload: { message: "Vendor Deactivated!" } });
    })
    .catch(e => console.log(e));
};

exports.getAllVendor = (req, res) => {
  Vendor.find({})
    .select({
      name: 1,
      email: 1,
      phone: 1,
      address: 1,
      url: 1,
      updated: 1,
      active: 1
    })
    .sort({ created: -1 })
    .then((vendors, err) => {
      if (err) {
        response = {
          success: false,
          payload: { message: "Error fetching Vendors" }
        };
      } else {
        if (!vendors) {
          res.json({
            success: false,
            payload: { message: "No Vendors Found!" }
          });
        }
        res.json({ success: true, payload: vendors });
      }
    })
    .catch(e => console.log(e));
};

exports.getVendor = (req, res) => {
  Vendor.findOne({ _id: req.params.vendorId })
    .then((vendor, err) => {
      if (err) {
        response = {
          success: false,
          payload: { message: "Error fetching Vendor" }
        };
      } else {
        if (!vendor) {
          res.json({
            success: false,
            payload: { message: "No Vendor Found!" }
          });
        }
        res.json({ success: true, payload: vendor });
      }
    })
    .catch(e => console.log(e));
};

exports.getAllVendorList = (req, res) => {
  Vendor.find({ active: true })
    .select({ name: 1 })
    .sort({ created: -1 })
    .then((vendors, err) => {
      if (err) {
        response = {
          success: false,
          payload: { message: "Error fetching data" }
        };
      } else {
        if (!vendors) {
          res.json({
            success: false,
            payload: { message: "No Vendors Found!" }
          });
        }
        res.json({ success: true, payload: vendors });
      }
    })
    .catch(e => console.log(e));
};
