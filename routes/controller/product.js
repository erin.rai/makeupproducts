const Product = require("../../models/product");
const validateProduct = require("../../helpers/validation/product");

exports.addProduct = (req, res) => {
  const { errors, isValid } = validateProduct(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }
  Product.findOne({
    name: req.body.name
  })
    .then(product => {
      if (product) {
        return res.status(404).json({
          success: false,
          payload: { message: "Product name already exists!" }
        });
      }
      const newProduct = new Product({
        name: req.body.name,
        model: req.body.model,
        colors: req.body.colors,
        brandId: req.body.brandId,
        description: req.body.description,
        imageUrl: req.body.imageUrl,
        categoryId: req.body.categoryId,
        subCategoryId: req.body.subCategoryId
      });

      newProduct
        .save()
        .then(prod => {
          res.json({
            success: true,
            payload: { message: "New Product added successfully!" }
          });
        })
        .catch(e => console.log(e));
    })
    .catch(e => console.log(e));
};

exports.uploadProductImages = (req, res) => {
  if (req.fileValidationError) {
    return res
      .status(400)
      .json({ success: false, payload: req.fileValidationError });
  }
  const url = [];
  if (req.files.length > 0) {
    req.files.map(i => {
      var uri = i.path.replace("public", "");
      url.push("http://localhost:5000" + uri);
    });
  }
  res.json({ success: true, payload: url });
};

exports.getProduct = (req, res) => {
  Product.findOne({ _id: req.params.productId })
    .then((prod, err) => {
      if (err) {
        res.json({
          success: false,
          payload: { message: "Error fetching product" }
        });
      } else {
        if (!prod) {
          res.json({
            success: false,
            payload: { message: "No Product found!" }
          });
        }
        res.json({ success: true, payload: prod });
      }
    })
    .catch(e => console.log(e));
};

exports.getAllProduct = (req, res) => {
  Product.find({}, { name: 1, status: 1 })
    .populate([
      { path: "categoryId", select: "name" },
      { path: "brandId", select: "name" }
    ])
    .sort({ created: -1 })
    .then((prods, err) => {
      if (err) {
        res.json({
          success: false,
          payload: { message: "Error fetching products" }
        });
      } else {
        if (!prods) {
          res.json({
            success: false,
            payload: { message: "No Products Found!" }
          });
        }
      }
      res.json({ success: true, payload: prods });
    })
    .catch(e => console.log(e));
};

exports.getAllProductList = (req, res) => {
  Product.find({}, { name: 1 })
    .sort({ created: -1 })
    .then((prods, err) => {
      if (err) {
        res.json({
          success: false,
          payload: { message: "Error fetching products" }
        });
      } else {
        if (!prods) {
          res.json({
            success: false,
            payload: { message: "No Products Found!" }
          });
        }
      }
      res.json({ success: true, payload: prods });
    })
    .catch(e => console.log(e));
};

exports.updateProduct = (req, res) => {
  const { errors, isValid } = validateProduct(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }
  Product.findOne({
    _id: req.params.productId
  })
    .then(product => {
      if (!product) {
        return res.status(404).json({
          success: false,
          payload: { message: "Product doesnot exists" }
        });
      }

      Product.updateOne(
        {
          _id: req.params.productId
        },
        {
          $set: {
            name: req.body.name,
            model: req.body.model,
            colors: req.body.colors,
            brandId: req.body.brandId,
            description: req.body.description,
            imageUrl: req.body.imageUrl,
            categoryId: req.body.categoryId,
            subCategoryId: req.body.subCategoryId,
            updated_at: Date.now()
          }
        }
      )
        .then((product, err) => {
          if (err) {
            res.json({
              success: false,
              payload: { message: "Error Updating Product" }
            });
          }
          res.json({
            success: true,
            payload: { mesasage: "Product updated successfully" }
          });
        })
        .catch(e => console.log(e));
    })
    .catch(e => console.log(e));
};

exports.deActivateProduct = (req, res) => {
  Product.findOne({
    _id: req.params.productId
  })
    .then(product => {
      if (!product) {
        res.status(404).json({
          success: false,
          payload: { message: "Prduct doesnot exist" }
        });
      }
      Product.updateOne(
        { _id: req.params.productId },
        { $set: { status: false, updated: Date.now() } }
      )
        .then((prod, err) => {
          if (err) {
            res.json({
              success: false,
              payload: { message: "Error Deactivating product" }
            });
          }
          res.json({
            success: true,
            payload: { message: "Product deactivated successfully" }
          });
        })
        .catch(e => console.log(e));
    })
    .catch(e => console.log(e));
};

exports.activateProduct = (req, res) => {
  Product.findOne({
    _id: req.params.productId
  })
    .then(product => {
      if (!product) {
        res.status(404).json({
          success: false,
          payload: { message: "Prduct doesnot exist" }
        });
      }
      Product.updateOne(
        { _id: req.params.productId },
        { $set: { status: true, updated: Date.now() } }
      )
        .then((prod, err) => {
          if (err) {
            res.json({
              success: false,
              payload: { message: "Error Activating product" }
            });
          }
          res.json({
            success: true,
            payload: { message: "Product activated successfully" }
          });
        })
        .catch(e => console.log(e));
    })
    .catch(e => console.log(e));
};

exports.countProduct = (req, res) => {
  Product.find().countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};

exports.countActiveProduct = (req, res) => {
  Product.find({ status: true }).countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};

exports.countInActiveProduct = (req, res) => {
  Product.find({ status: false }).countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};
