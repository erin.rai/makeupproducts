const Category = require("../../models/category");
const validateCategory = require("../../helpers/validation/category");
const validateSubCategory = require("../../helpers/validation/sub_category");

exports.getAllCategory = (req, res) => {
  Category.find({}, { name: 1, subcategory: 1, status: 1 })
    .sort({ created: -1 })
    .then((cats, err) => {
      if (err) {
        res.json({
          success: false,
          payload: { message: "Error fetching data" }
        });
      } else {
        if (!cats) {
          res.json({
            success: false,
            payload: { message: "No Brands Found!" }
          });
        }
        res.json({ success: true, payload: cats });
      }
    })
    .catch(e => console.log(e));
};

exports.countCategory = (req, res) => {
  Category.find().countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};

exports.countActiveCategory = (req, res) => {
  Category.find({ status: true }).countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};

exports.countInActiveCategory = (req, res) => {
  Category.find({ status: false }).countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};

exports.getAllCategoryList = (req, res) => {
  // Category.find(
  //   // {
  //   //   name: 1,
  //   //   "subcategory.name": 1,
  //   //   "subcategory._id": 1,
  //   //   "subcategory.status": 1
  //   // }
  // )
  //   .sort({ created: -1 })
  Category.aggregate([
    { $match: { status: true } },

    { $unwind: "$subcategory" },
    { $match: { "subcategory.status": true } },
    {
      $group: {
        _id: "$_id",
        name: { $first: "$name" },
        subcategory: {
          $push: {
            _id: "$subcategory._id",
            name: "$subcategory.name",
            status: "$subcategory.status"
          }
        }
      }
    }
  ])
    .then((cats, err) => {
      if (err) {
        response = {
          success: false,
          payload: { message: "Error fetching data" }
        };
      } else {
        if (!cats) {
          res.json({
            success: false,
            payload: { message: "No Brands Found!" }
          });
        }
        res.json({ success: true, payload: cats });
      }
    })
    .catch(e => console.log(e));
};

exports.getCategory = (req, res) => {
  Category.findOne({ _id: req.params.categoryId })
    .then((cat, err) => {
      if (err) {
        res.json({
          success: false,
          payload: { message: "Error fetching data" }
        });
      } else {
        if (!cat) {
          res.json({
            success: false,
            payload: { message: "No Category Found" }
          });
        }
        res.json({ success: true, payload: cat });
      }
    })
    .catch(e => console.log(e));
};

exports.addCategory = (req, res) => {
  const { errors, isValid } = validateCategory(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }
  Category.findOne({
    name: req.body.name
  }).then(category => {
    if (category) {
      return res.status(404).json({
        success: false,
        payload: { message: "Category name already exists!" }
      });
    }
    console.log(req.body.name);
    const newCategory = new Category({
      name: req.body.name
    });
    newCategory
      .save()
      .then(cat =>
        res.json({
          success: true,
          payload: { message: "New Category added successfully" }
        })
      )
      .catch(e => console.log(e));
  });
};

exports.addSubcategory = (req, res) => {
  Category.findOne({
    $and: [
      {
        _id: req.params.categoryId
      },
      {
        status: true
      }
    ]
  }).then(category => {
    if (!category) {
      return res.status(404).json({
        success: false,
        payload: { message: "Category doesnot exist!" }
      });
    }
    const { errors, isValid } = validateSubCategory(req.body);
    if (!isValid) {
      return res.status(400).json({ success: false, payload: errors });
    }
    Category.findOne({
      _id: req.params.categoryId,
      "subcategory.name": req.body.name
    })
      .then(subcat => {
        if (subcat) {
          return res.status(404).json({
            success: false,
            payload: { message: "Subcategory name already exist!" }
          });
        }
        let subcategory = {
          name: req.body.name
        };
        Category.updateOne(
          { _id: req.params.categoryId },
          { $push: { subcategory: subcategory } }
        ).then(category => {
          res.json({
            success: true,
            payload: { message: "Sub Category added successfully" }
          });
        });
      })
      .catch(e => console.log(e));
  });
};

exports.updateCategory = (req, res) => {
  const { errors, isValid } = validateCategory(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }
  Category.findOne({
    $and: [
      {
        _id: req.params.categoryId
      },
      {
        status: true
      }
    ]
  }).then(category => {
    if (!category) {
      return res.status(404).json({
        success: false,
        payload: { message: "Category name doesnot exist!" }
      });
    }
    Category.updateOne(
      { _id: req.params.categoryId },
      { $set: { name: req.body.name, updated: Date.now() } }
    )
      .then((category, err) => {
        if (err) {
          res.json({
            success: false,
            payload: { message: "Error Updating data" }
          });
        }
        res.json({
          success: true,
          payload: { message: "Category updated successfully" }
        });
      })
      .catch(e => console.log(e));
  });
};

exports.activateCategory = (req, res) => {
  Category.findOne({
    $and: [
      {
        _id: req.params.categoryId
      }
    ]
  })
    .then(category => {
      if (!category) {
        res.status(404).json({
          success: false,
          payload: { message: "Category name doesnot exist!" }
        });
      }
      Category.updateOne(
        { _id: req.params.categoryId },
        { $set: { status: true, updated: Date.now() } }
      )
        .then((category, err) => {
          if (err) {
            res.json({
              success: false,
              payload: { message: "Error Updating data" }
            });
          }
          res.json({
            success: true,
            payload: { message: "Category activated successfully" }
          });
        })
        .catch(e => console.log(e));
    })
    .catch(e => console.log(e));
};
exports.deactivateCategory = (req, res) => {
  Category.findOne({
    $and: [
      {
        _id: req.params.categoryId
      },
      {
        status: true
      }
    ]
  })
    .then(category => {
      if (!category) {
        return res.status(404).json({
          success: false,
          payload: { message: "Category name doesnot exist!" }
        });
      }
      Category.updateOne(
        { _id: req.params.categoryId },
        { $set: { status: false, updated: Date.now() } }
      )
        .then((category, err) => {
          if (err) {
            response = {
              success: false,
              payload: { message: "Error Updating data" }
            };
          }
          res.json({
            success: true,
            payload: { message: "Category deactivated successfully" }
          });
        })
        .catch(e => console.log(e));
    })
    .catch(e => console.log(e));
};

exports.updateSubcategory = (req, res) => {
  Category.findOne({
    $and: [
      {
        _id: req.params.categoryId
      },
      {
        status: true
      }
    ]
  })
    .then(category => {
      if (!category) {
        return res.status(404).json({
          success: false,
          payload: { message: "Category name doesnot exist!" }
        });
      }
      const { errors, isValid } = validateSubCategory(req.body);
      if (!isValid) {
        return res.status(400).json({ success: false, payload: errors });
      }
      Category.findOne({
        _id: req.params.categoryId,
        "subcategory._id": req.body.subId
      })
        .then(subcat => {
          if (!subcat) {
            return res.status(404).json({
              success: false,
              payload: { message: "Subcategory does not exist!" }
            });
          }
          Category.updateOne(
            {
              _id: req.params.categoryId,
              "subcategory._id": req.body.subId
            },
            {
              $set: {
                "subcategory.$.name": req.body.name,
                "subcategory.$.updated": Date.now()
              }
            }
          )
            .then((category, err) => {
              if (err) {
                response = {
                  success: false,
                  payload: { message: "Error Updating data" }
                };
              }
              res.json({
                success: true,
                payload: { message: "Sub Category updated successfully" }
              });
            })
            .catch(e => console.log(e));
        })
        .catch(e => console.log(e));
    })
    .catch(e => console.log);
};

exports.deactivateSubcategory = (req, res) => {
  Category.findOne({
    _id: req.params.categoryId
  })
    .then(category => {
      if (!category) {
        return res.status(404).json({
          success: false,
          payload: { message: "Category doesnot exist!" }
        });
      }
      Category.findOne({
        _id: req.params.categoryId,
        "subcategory._id": req.body.subId
      })
        .then(subcat => {
          if (!subcat) {
            return res.status(404).json({
              success: false,
              payload: { message: "Subcategory does not exist!" }
            });
          }
          let subcategory = {
            name: req.body.name
          };
          Category.updateOne(
            { _id: req.params.categoryId, "subcategory._id": req.body.subId },
            {
              $set: {
                "subcategory.$.status": false,
                "subcategory.$.updated": Date.now()
              }
            }
          )
            .then((category, err) => {
              if (err) {
                response = {
                  success: false,
                  payload: { message: "Error Updating data" }
                };
              }
              res.json({
                success: true,
                payload: { message: "Subcategory Deactivated successfully" }
              });
            })
            .catch(e => console.log(e));
        })
        .catch(e => console.log(e));
    })
    .catch(e => console.log(e));
};

exports.activateSubcategory = (req, res) => {
  Category.findOne({
    _id: req.params.categoryId
  }).then(category => {
    if (!category) {
      return res.status(404).json({
        success: false,
        payload: { message: "Category name doesnot exist!" }
      });
    }
    Category.findOne({
      _id: req.params.categoryId,
      "subcategory._id": req.body.subId
    })
      .then(subcat => {
        if (!subcat) {
          return res.status(404).json({
            success: false,
            payload: { message: "Subcategory does not exist!" }
          });
        }
        let subcategory = {
          name: req.body.name
        };
        Category.updateOne(
          { _id: req.params.categoryId, "subcategory._id": req.body.subId },
          {
            $set: {
              "subcategory.$.status": true,
              "subcategory.$.updated": Date.now()
            }
          }
        )
          .then((category, err) => {
            if (err) {
              response = {
                success: false,
                payload: { message: "Error Updating data" }
              };
            }
            res.json({
              success: true,
              payload: { message: "Subcategory Deactivated successfully" }
            });
          })
          .catch(e => console.log(e));
      })
      .catch(e => console.log(e));
  });
};
