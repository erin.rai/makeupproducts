const Brand = require("../../models/brand");
const validateBrand = require("../../helpers/validation/brand");

exports.uploadBrandImage = (req, res) => {
  if (req.fileValidationError) {
    return res
      .status(400)
      .json({ success: false, payload: req.fileValidationError });
  }
  var uri = req.file.path.replace("public", "");
  var url = "http://localhost:5000" + uri;
  res.json({ success: true, payload: { url } });
};

exports.addBrand = (req, res) => {
  const { errors, isValid } = validateBrand(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }

  Brand.findOne({
    name: req.params.name
  }).then(brand => {
    if (brand) {
      return res.status(404).json({
        success: false,
        payload: { message: "Brand name already exists!" }
      });
    }
    const newBrand = new Brand({
      name: req.body.name,
      url: req.body.url,
      imageUrl: req.body.imageUrl,
      description: req.body.description
    });
    newBrand
      .save()
      .then(brand =>
        res.json({
          success: true,
          payload: { message: "New Brand added successfully" }
        })
      )
      .catch(e => console.log(e));
  });
};

exports.countBrand = (req, res) => {
  Brand.find().countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};
exports.countActiveBrand = (req, res) => {
  Brand.find({ active: true }).countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};
exports.countInActiveBrand = (req, res) => {
  Brand.find({ active: false }).countDocuments(function(err, count) {
    if (err) {
      console.log(err);
    } else {
      res.json({ success: true, payload: { count } });
    }
  });
};
exports.updateBrand = (req, res) => {
  const { errors, isValid } = validateBrand(req.body);
  if (!isValid) {
    return res.status(400).json({ success: false, payload: errors });
  }
  Brand.findOneAndUpdate(
    { _id: req.params.brandId },
    {
      $set: {
        name: req.body.name,
        url: req.body.url,
        imageUrl: req.body.imageUrl,
        description: req.body.description,
        updated: Date.now()
      }
    },
    { new: true, useFindAndModify: false }
  )
    .then(brand => {
      res.json({ success: true, payload: { message: "Brand updated!" } });
    })
    .catch(e => console.log(e));
};

exports.activateBrand = (req, res) => {
  Brand.findOneAndUpdate(
    { _id: req.params.brandId },
    {
      $set: {
        active: true
      }
    },
    { new: true, useFindAndModify: false }
  )
    .then(brand => {
      res.json({ success: true, payload: { message: "Brand updated!" } });
    })
    .catch(e => console.log(e));
};
exports.deactivateBrand = (req, res) => {
  Brand.findOneAndUpdate(
    { _id: req.params.brandId },
    {
      $set: {
        active: false
      }
    },
    { new: true, useFindAndModify: false }
  )
    .then(brand => {
      res.json({ success: true, payload: { message: "Brand updated!" } });
    })
    .catch(e => console.log(e));
};

exports.getAllBrand = (req, res) => {
  Brand.find({})
    .select({ name: 1, url: 1, updated: 1, active: 1, updated: 1 })
    .sort({ created: -1 })
    .then((brands, err) => {
      if (err) {
        response = {
          success: false,
          payload: { message: "Error fetching data" }
        };
      } else {
        if (!brands) {
          res.json({
            success: false,
            payload: { message: "No Brands Found!" }
          });
        }
        res.json({ success: true, payload: brands });
      }
    })
    .catch(e => console.log(e));
};

exports.getBrand = (req, res) => {
  Brand.findOne({ _id: req.params.brandId })
    .then((brand, err) => {
      if (err) {
        response = {
          success: false,
          payload: { message: "Error fetching data" }
        };
      } else {
        if (!brand) {
          res.json({ success: false, payload: { message: "No Brand Found!" } });
        }
        res.json({ success: true, payload: brand });
      }
    })
    .catch(e => console.log(e));
};

exports.getAllBrandList = (req, res) => {
  Brand.find({ active: true })
    .select({ name: 1 })
    .sort({ created: -1 })
    .then((brands, err) => {
      if (err) {
        response = {
          success: false,
          payload: { message: "Error fetching data" }
        };
      } else {
        if (!brands) {
          res.json({
            success: false,
            payload: { message: "No Brands Found!" }
          });
        }
        res.json({ success: true, payload: brands });
      }
    })
    .catch(e => console.log(e));
};
