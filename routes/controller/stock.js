const Stock = require("../../models/stock");

exports.getAllStock = (req, res) => {
  Stock.find({})
    .populate([{ path: "productId", select: "name" }])
    .sort({ created: -1 })
    .then((stocks, err) => {
      if (err) {
        res.json({
          success: false,
          payload: { message: "Error fetching stocks" }
        });
      } else {
        if (!stocks) {
          res.json({
            success: false,
            payload: { message: "No Stocks found" }
          });
        }
        res.json({ success: true, payload: stocks });
      }
    })
    .then(err => console.log(err));
};

exports.getStock = (req, res) => {
  Stock.find({ _id: req.params.stockId })
    .populate([{ path: "productId", select: "name" }])
    .sort({ created: -1 })
    .then((stocks, err) => {
      if (err) {
        res.json({
          success: false,
          payload: { message: "Error fetching stock" }
        });
      } else {
        if (!stocks) {
          res.json({
            success: false,
            payload: { message: "No Stock found" }
          });
        }
        res.json({ success: true, payload: stocks });
      }
    })
    .then(err => console.log(err));
};
